package carware;

import apoio.ConexaoBD;
import apoio.Logs;
import dao_hibernate.LoginDAO;
import javax.swing.JOptionPane;
import tela.DlgLogin;
import tela.FrmSplashScreen;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class Carware {

    public static void main(String[] args) {
        mudarLookAndFeel();
        splashScreen();
        Logs.criarLog();
        conectarAoBanco();
    }

    public static void conectarAoBanco() {
        if (ConexaoBD.getInstance().getConnection() != null) {
            DlgLogin login = new DlgLogin(null, true);
            login.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Deu problema!");
        }
    }

    public static void mudarLookAndFeel() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DlgLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DlgLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DlgLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DlgLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public static void splashScreen() {
        FrmSplashScreen splash = new FrmSplashScreen();

        LoginDAO dao = new LoginDAO();
        dao.consultaId(1);

        splash.dispose();
    }
}
