package apoio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class formataData {

    public static String formatarData(String dataN) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date data;
        try {
            data = formato.parse(dataN);
            formato.applyPattern("dd/MM/yyyy");
            String dataFormatada = formato.format(data);
            return dataFormatada;
        } catch (ParseException ex) {
            return ex.toString();
        }
    }
}
