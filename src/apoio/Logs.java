package apoio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class Logs {

    private static Path caminhoParaArquivo = Paths.get("logs/" + java.time.LocalDate.now() + ".csv");

    public static void criarLog() {
        verificarSeArquivoExiste();
    }

    public static boolean verificarSeArquivoExiste() {
        boolean arquivoExiste = false;
        try {
            arquivoExiste = Files.exists(caminhoParaArquivo);
            if (!arquivoExiste) {
                Path createdFilePath = Files.createFile(caminhoParaArquivo);
                System.out.println("Arquivo de Log CSV criado em: " + createdFilePath);
                enviarParaArquivo("ID,Tabela,Descrição,Usuário,Horário\n");
                arquivoExiste = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return arquivoExiste;
    }

    public static void escrever(int id, String tabela, String descricao, String usuario) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date hora = Calendar.getInstance().getTime();
        String horaAtualFormatada = sdf.format(hora);

        String texto = ""
                + "\" " + id + " \","
                + "\" " + tabela + " \","
                + "\" " + descricao + " \","
                + "\" " + usuario + " \","
                + "\" " + horaAtualFormatada + " \"\n";

        enviarParaArquivo(texto);
    }

    public static void enviarParaArquivo(String texto) {
        if (verificarSeArquivoExiste()) {
            try {
                byte[] bytes = texto.getBytes();
                Files.write(caminhoParaArquivo, bytes, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
