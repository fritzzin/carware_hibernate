/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apoio;

import java.awt.Color;
import java.util.logging.*;
import javax.swing.JLabel;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class ThreadMessage implements Runnable {

    JLabel l = null;
    String message = null;

    public ThreadMessage(JLabel label, String message) {
        this.l = label;
        this.message = message;
    }

    public void run() {
        l.setText(message);
        l.setForeground(Color.BLACK);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
        l.setText("");

    }
}
