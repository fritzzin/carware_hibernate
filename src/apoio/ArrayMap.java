package apoio;

import apoio.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lucas.assis
 */
public class ArrayMap {
    String nome;
    double porcentage;
    
    public ArrayMap(String nome, double porcentage){
        this.nome = nome;
        this.porcentage = porcentage;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPorcentage() {
        return porcentage;
    }

    public void setPorcentage(int porcentage) {
        this.porcentage = porcentage;
    }
    
}
