package apoio;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lucas.assis
 */
import apoio.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class LineChartEx extends JInternalFrame {

    private static final long serialVersionUID = 1L;

    public LineChartEx() {

    }

    public JInternalFrame LineChartEx(String chartTitle, ArrayList<ArrayMap> array) {
        JButton btnSair = new JButton("SAIR");
        btnSair.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                teste();
            }
        });
        btnSair.setLocation(1, 1);

        // This will create the dataset
        PieDataset dataset = createDataset(array);
        // based on the dataset we create the chart
        JFreeChart chart = createChart(dataset, chartTitle);
        // we put the chart into a panel
        ChartPanel chartPanel = new ChartPanel(chart);
        // default size
        chartPanel.setPreferredSize(new java.awt.Dimension(720, 480));
        // add it to our application
        setContentPane(chartPanel);
        return this;

    }

    public void teste(){
        this.dispose();
    }
    /**
     * Creates a sample dataset
     */
    private PieDataset createDataset(ArrayList<ArrayMap> array) {

        DefaultPieDataset result = new DefaultPieDataset();

        for (int i = 0; i < array.size(); i++) {
            result.setValue(array.get(i).nome, array.get(i).porcentage);
        }
        return result;

    }

    /**
     * Creates a chart
     */
    private JFreeChart createChart(PieDataset dataset, String title) {

        JFreeChart chart = ChartFactory.createPieChart(
                title, // chart title
                dataset, // data
                true, // include legend
                true,
                false
        );

        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        return chart;

    }
}
