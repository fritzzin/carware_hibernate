/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apoio;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author lucas
 */
public class EmailSend {
    //path = caminho(diretorio) do pdf
    //destinatario = email de destino
    public static boolean SendEmailOrcamento(String path, String destinatario) {
        String meuEmail = "carwaresoftware@gmail.com";
        String senha = "Grupo(carware";
        MultiPartEmail email = new MultiPartEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(meuEmail, senha));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(destinatario);
            email.setSubject("Os de seu veículo Carware");
            email.addTo(destinatario);
            email.setMsg("Segue em anexo o orcamento de seu veículo \n"
                    + "Caso queira desistir do servico entre em contato com o empresa!\n");
            EmailAttachment anexo = new EmailAttachment();
            anexo.setPath(path);
            anexo.setName("Orçamento.pdf");
            email.attach(anexo);
            email.send();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public static boolean SendEmailServico(String path, String destinatario) {
        String meuEmail = "carwaresoftware@gmail.com";
        String senha = "Grupo(carware";
        MultiPartEmail email = new MultiPartEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(meuEmail, senha));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(destinatario);
            email.addTo(destinatario);
            email.setSubject("O Serviço foi concluido!");
            email.setMsg("O serviço em seu veículo foi concluido\n"
                    + "Seu veiculo está pronto para a retirada a qualquer momento em horario comercial!");
            EmailAttachment anexo = new EmailAttachment();
            anexo.setPath(path);
            anexo.setName("Relatorio_Total.pdf");
            email.attach(anexo);
            email.send();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public static boolean SendEmailOrcamento(String destinatario) {
        String meuEmail = "carwaresoftware@gmail.com";
        String senha = "Grupo(carware";
        SimpleEmail email = new SimpleEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(meuEmail, senha));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(destinatario);
            email.setSubject("Os de seu veículo Carware");
            email.setMsg("Segue em anexo o orcamento de seu veículo \n"
                    + "Caso queira desistir do servico entre em contato com o empresa!\n");
            email.addTo(destinatario);
            email.send();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public static boolean SendEmailServico(String destinatario) {
        String meuEmail = "carwaresoftware@gmail.com";
        String senha = "Grupo(carware";
        SimpleEmail email = new SimpleEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(meuEmail, senha));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(destinatario);
            email.setSubject("O Serviço concluido!");
            email.setMsg("O serviço em seu veículo foi concluido\n"
                    + "Seu veiculo está pronto para a retirada a qualquer momento em horario comercial!");
            email.addTo(destinatario);
            email.send();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
