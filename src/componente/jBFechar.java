/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componente;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author fritzzin
 */
public class jBFechar extends JButton {

    public jBFechar() {
        this.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Component component = (Component) ae.getSource();
                JFrame frame = (JFrame) SwingUtilities.getRoot(component);
                frame.dispose();
            }
        });
    }
}
