/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componente;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFormattedTextField;

/**
 *
 * @author lucas.assis
 */
public class JftTelefone extends JFormattedTextField{
    public JftTelefone(){
        try {
            this.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)#####-####")));
        } catch (ParseException ex) {
            Logger.getLogger(JftData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
