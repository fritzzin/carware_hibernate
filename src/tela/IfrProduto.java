package tela;

import apoio.Logs;
import apoio.ThreadMessage;
import apoio.Validacao;
import classes_hibernate.Funcionario;
import classes_hibernate.Produto;
import dao_hibernate.ProdutoDAO;
import java.awt.Color;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class IfrProduto extends javax.swing.JInternalFrame {

    // Variaveis
    String modo;
    int idProduto;
    ProdutoDAO dao;
    Validacao validacao;

    // Variaveis de lazy load
    int intervalo = 40;
    int inicio = 0;
    int fim = intervalo;
    boolean chegouNoFinal = false;
    boolean chegouNoInicio = true;

    // Variaveis para log
    Funcionario usuario;
    String nome = "";
    String nomeAnterior = "";
    String tabela = "Produto";

    public IfrProduto(Funcionario f) {
        initComponents();
        dao = new ProdutoDAO();

        popularTabela();
        this.setTitle("Cadastro de Produtos");

        this.usuario = f;
        modo = "cadastro";
        jLabelModo.setText("Modo: Cadastro");

        abas.setSelectedIndex(1);

        if (f.getFuncao().getNome().equals("Atendente")) {
            abas.setEnabled(false);
            abas.setSelectedIndex(1);
            btnEditar.setEnabled(false);
            btnSalvar.setEnabled(false);
            btnRemover.setEnabled(false);
        }

        // Pegar evento do scrollbar para fazer a carga dos registros
        final JScrollPane scrollPane = jScrollPane1;
        scrollPane.setVisible(true);
        scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                if (!e.getValueIsAdjusting()) {
                    JScrollBar scrollBar = (JScrollBar) e.getAdjustable();
                    int extent = scrollBar.getModel().getExtent();
                    int maximum = scrollBar.getModel().getMaximum();
                    int minimum = scrollBar.getModel().getMinimum();

                    // scroll para CIMA
                    if (extent + e.getValue() == maximum) {
                        popularTabela("frente");
                        chegouNoInicio = false;

                        if (!chegouNoFinal) {
                            scrollBar.setValue(extent / 2);
                        }
                        // scroll para BAIXO
                    } else if (e.getValue() == 0) {
                        if (!chegouNoInicio) {
                            popularTabela("atras");
                            scrollBar.setValue(extent / 2);
                        }
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        abas = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        txtValorCompra = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtValorVenda = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtQuantidade = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabelModo = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtConsultaNome = new javax.swing.JTextField();
        btnConsultar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        comboPreco = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableProdutos = new javax.swing.JTable();
        btnLimparCriterio = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        jLMensagem = new componente.jLMensagem();
        btnRemover = new javax.swing.JButton();

        abas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                abasStateChanged(evt);
            }
        });

        jLabel1.setText("Nome:*");

        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNomeKeyTyped(evt);
            }
        });

        txtValorCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValorCompraKeyTyped(evt);
            }
        });

        jLabel2.setText("Valor de Compra:*");

        txtValorVenda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValorVendaKeyTyped(evt);
            }
        });

        jLabel3.setText("Valor de Venda:*");

        txtQuantidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtQuantidadeKeyTyped(evt);
            }
        });

        jLabel4.setText("Quantidade:*");

        jLabelModo.setText("Modo: Cadastro");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtValorCompra, javax.swing.GroupLayout.DEFAULT_SIZE, 708, Short.MAX_VALUE)
                                .addComponent(txtValorVenda)
                                .addComponent(txtNome))
                            .addComponent(txtQuantidade, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 708, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabelModo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtValorVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 214, Short.MAX_VALUE)
                .addComponent(jLabelModo)
                .addContainerGap())
        );

        abas.addTab("Cadastro/Edição", jPanel1);

        jLabel5.setText("Nome:");

        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        jLabel6.setText("Valor Venda:");

        comboPreco.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Valores padrão", "Maior para menor", "Menor para maior"}));
        comboPreco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPrecoActionPerformed(evt);
            }
        });

        tableProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableProdutos);

        btnLimparCriterio.setText("X");
        btnLimparCriterio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparCriterioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtConsultaNome, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimparCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboPreco, 0, 181, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultar)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConsultar)
                    .addComponent(txtConsultaNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(comboPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLimparCriterio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        abas.addTab("Consulta", jPanel2);

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnRemover.setText("Remover");
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFechar)
                .addGap(18, 18, 18)
                .addComponent(btnRemover)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalvar)
                .addContainerGap())
            .addComponent(abas)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(abas, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalvar)
                        .addComponent(btnEditar)
                        .addComponent(btnFechar)
                        .addComponent(btnRemover))
                    .addComponent(jLMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        if (modo.equals("edicao")) {
            abas.setSelectedIndex(1);
            abas.setEnabled(true);
            btnRemover.setEnabled(true);
            btnFechar.setText("Fechar");

            trocarModo();
            limparCampos();
        } else {
            this.dispose();
        }

    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        trocarModo();

        // Desabilitar botões
        abas.setSelectedIndex(0);
        abas.setEnabled(false);
        btnRemover.setEnabled(false);
        btnEditar.setEnabled(false);
        btnFechar.setText("Cancelar");

        pegarIdSelecionado();

        Produto produto = (Produto) dao.consultaId(this.idProduto);
        this.nome = produto.getNome();
        String valorCompra = String.valueOf(produto.getValorpago());
        String valorVenda = String.valueOf(produto.getValorvenda());
        String quantidade = String.valueOf(produto.getQuantidade());

        nomeAnterior = nome;
        txtNome.setText(nome);
        txtValorCompra.setText(valorCompra);
        txtValorVenda.setText(valorVenda);
        txtQuantidade.setText(quantidade);

        txtNome.requestFocus();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        this.nome = txtNome.getText();
        String valorVendaTexto = txtValorVenda.getText();
        String valorCompraTexto = txtValorCompra.getText();
        String quantidadeTexto = txtQuantidade.getText();

        // verificações
        boolean nomeVazio = Validacao.vazio(nome);
        boolean valorVendaVazio = Validacao.vazio(valorVendaTexto);
        boolean valorCompraVazio = Validacao.vazio(valorCompraTexto);
        boolean quantidadeVazio = Validacao.vazio(quantidadeTexto);

        if (!nomeVazio && !valorVendaVazio && !valorCompraVazio && !quantidadeVazio) {
            Double valorVenda = Double.parseDouble(valorVendaTexto);
            Double valorCompra = Double.parseDouble(valorCompraTexto);
            Double quantidade = Double.parseDouble(quantidadeTexto);

            Produto produto = new Produto();
            produto.setNome(this.nome);
            produto.setValorvenda(BigDecimal.valueOf(valorVenda));
            produto.setValorpago(BigDecimal.valueOf(valorCompra));
            produto.setQuantidade(BigDecimal.valueOf(quantidade));

            if (modo.equals("edicao")) {
                produto.setId(this.idProduto);
            }

            if (dao.salvar(produto)) {
                mensagem("Salvo com sucesso");

                if (modo.equals("cadastro")) {
                    txtNome.requestFocus();
                }

                limparCampos();
                popularTabela();

                // Reativar botões e trocar labels
                if (modo.equals("edicao")) {
                    abas.setSelectedIndex(1);
                    abas.setEnabled(true);
                    btnRemover.setEnabled(true);
                    btnEditar.setEnabled(true);
                    btnFechar.setText("Fechar");

                    trocarModo();
                    enviarParaLog("edicao");
                } else {
                    this.idProduto = 0;
                    enviarParaLog("insercao");
                }
            } else {
                mensagem("Ocorreu um erro ao salvar produto!");
            }
        } else {
            pintarCamposDeBranco();
            mensagem("Preencha os campos corretamente");
            if (nomeVazio) {
                txtNome.setBackground(Color.PINK);
            }
            if (valorCompraVazio) {
                txtValorCompra.setBackground(Color.PINK);
            }
            if (valorVendaVazio) {
                txtValorVenda.setBackground(Color.PINK);
            }
            if (quantidadeVazio) {
                txtQuantidade.setBackground(Color.PINK);
            }
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        popularTabela();
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
        pegarIdSelecionado();

        Produto produto = (Produto) dao.consultaId(this.idProduto);
        this.nome = produto.getNome();

        if (dao.deletar(produto)) {
            mensagem("Removido com sucesso!");
            enviarParaLog("remocao");
            popularTabela();
        } else {
            mensagem("Erro ao remover Produto!");
        }
    }//GEN-LAST:event_btnRemoverActionPerformed

    private void abasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_abasStateChanged
        if (abas.getSelectedIndex() == 0) {
            btnSalvar.setEnabled(true);
            btnRemover.setEnabled(false);
            btnEditar.setEnabled(false);
        } else {
            btnSalvar.setEnabled(false);
            btnRemover.setEnabled(true);
            btnEditar.setEnabled(true);
        }
    }//GEN-LAST:event_abasStateChanged

    private void comboPrecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPrecoActionPerformed
        btnConsultarActionPerformed(evt);
    }//GEN-LAST:event_comboPrecoActionPerformed

    private void btnLimparCriterioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparCriterioActionPerformed
        txtConsultaNome.setText("");
        popularTabela();
    }//GEN-LAST:event_btnLimparCriterioActionPerformed

    private void txtValorCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorCompraKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtValorCompra);
    }//GEN-LAST:event_txtValorCompraKeyTyped

    private void txtValorVendaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorVendaKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtValorVenda);
    }//GEN-LAST:event_txtValorVendaKeyTyped

    private void txtQuantidadeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuantidadeKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtQuantidade);
    }//GEN-LAST:event_txtQuantidadeKeyTyped

    private void txtNomeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyTyped
        enterParaSalvar(evt);
    }//GEN-LAST:event_txtNomeKeyTyped

    public void enterParaSalvar(java.awt.event.KeyEvent evt) {
        if (evt.getKeyChar() == '\n') {
            btnSalvarActionPerformed(null);
        }
    }

    public void enviarParaLog(String evento) {
        String texto = "";
        if (evento.equals("insercao")) {
            texto = "\"\"" + this.nome + "\"\" Inserido";
        }
        if (evento.equals("edicao")) {
            texto = "\"\"" + this.nomeAnterior + "\"\" Modificado";
        }
        if (evento.equals("remocao")) {
            texto = "\"\"" + this.nome + "\"\" Removido";
        }

        Logs.escrever(this.idProduto, this.tabela, texto, this.usuario.getLogin().getLogin());
    }

    public void digitarApenasNumeros(KeyEvent evt, JTextField txt) {
        String caracters = "0123456789.";
        String ponto = ".";
        String texto = txt.getText();

        if (texto.contains(ponto)) {
            if (evt.getKeyChar() == 46) { // 46 = code ASCII para "."
                evt.consume();
            }
        }

        if (!caracters.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }

    private void trocarModo() {
        if (modo.equals("edicao")) {
            modo = "cadastro";
            jLabelModo.setText("Modo: Cadastro");
        } else {
            modo = "edicao";
            jLabelModo.setText("Modo: Edição");
        }
    }

    private void mensagem(String texto) {
        Runnable run = new ThreadMessage(jLMensagem, texto);
        new Thread(run).start();
    }

    private void pegarIdSelecionado() {
        int row = tableProdutos.getSelectedRow();
        this.idProduto = (int) tableProdutos.getValueAt(row, 0);
    }

    private void limparCampos() {
        txtNome.setText("");
        txtQuantidade.setText("");
        txtValorCompra.setText("");
        txtValorVenda.setText("");

        pintarCamposDeBranco();
    }

    public void pintarCamposDeBranco() {
        txtNome.setBackground(Color.WHITE);
        txtValorCompra.setBackground(Color.WHITE);
        txtValorVenda.setBackground(Color.WHITE);
        txtQuantidade.setBackground(Color.WHITE);
    }

    private void popularTabela(String direcao) {
        String criterio = txtConsultaNome.getText();
        String ordem = "";

        if (direcao.equals("frente")) {
            this.inicio = this.fim;
            this.fim += intervalo;
            dao.listarEntreValores(tableProdutos, criterio, ordem, inicio, fim, this);

        } else {
            if (this.inicio <= 0) {
                this.inicio = 0;
                this.fim = intervalo;
                this.chegouNoInicio = true;
                dao.listarEntreValores(tableProdutos, criterio, ordem, inicio, fim, this);
            } else {
                this.inicio -= intervalo;
                this.fim -= intervalo;
                dao.listarEntreValores(tableProdutos, criterio, ordem, inicio, fim, this);
            }
        }
    }

    private void popularTabela() {
        String criterio = txtConsultaNome.getText();
        String ordem = "";

        switch (comboPreco.getSelectedIndex()) {
            case 0:
                ordem = "";
                break;
            case 1:
                ordem = "desc";
                break;
            case 2:
                ordem = "asc";
                break;
        }
        this.inicio = 0;
        this.fim = intervalo;

        dao.listarEntreValores(tableProdutos, criterio, ordem, inicio, fim, this);
    }

    public void setChegouNoFinal(boolean valor) {
        this.chegouNoFinal = valor;
    }

    public void setChegouNoInicio(boolean valor) {
        this.chegouNoInicio = valor;
    }

    public void setInicioEFim(int valor1, int valor2) {
        this.inicio = valor1;
        this.fim = valor2;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane abas;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnLimparCriterio;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> comboPreco;
    private componente.jLMensagem jLMensagem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelModo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableProdutos;
    private javax.swing.JTextField txtConsultaNome;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtQuantidade;
    private javax.swing.JTextField txtValorCompra;
    private javax.swing.JTextField txtValorVenda;
    // End of variables declaration//GEN-END:variables
}
