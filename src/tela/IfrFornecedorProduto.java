package tela;

import apoio.Formatacao;
import apoio.Logs;
import apoio.ThreadMessage;
import apoio.Validacao;
import classes_hibernate.Fornecedor;
import classes_hibernate.Fornecedorproduto;
import classes_hibernate.Funcionario;
import classes_hibernate.Produto;
import dao_hibernate.FornecedorProdutoDAO;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTextField;

/**
 *
 * @author Augusto Fritz; Lucas de Assis
 */
public class IfrFornecedorProduto extends javax.swing.JInternalFrame {

    String modo;
    FornecedorProdutoDAO dao;
    int idFornecedorProduto;
    Validacao validacao;

    Fornecedor fornecedor;
    Produto produto;

    // Variáveis para log
    Funcionario usuario;
    String nome = "";
    String nomeAnterior = "";
    String tabela = "FornecedorProduto";

    public IfrFornecedorProduto(Funcionario usuario) {
        initComponents();
        dao = new FornecedorProdutoDAO();

        popularTabela();
        this.setTitle("Contas");

        this.usuario = usuario;
        this.modo = "cadastro";
        labelModo.setText("Modo: Cadastro");

        abas.setSelectedIndex(1);

        Formatacao.formatarData(txtDataDeCompra);
        Formatacao.formatarData(txtDataDeVencimento);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        abas = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtFornecedor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtProduto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtDataDeCompra = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        txtQuantidade = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtValorUnitario = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        checkboxStatus = new javax.swing.JCheckBox();
        labelModo = new javax.swing.JLabel();
        txtDataDeVencimento = new javax.swing.JFormattedTextField();
        btnSelecionarFornecedor = new javax.swing.JButton();
        btnSelecionarProduto = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableProdutos = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        comboPreco = new javax.swing.JComboBox<>();
        btnConsultar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        btnRemover = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        jLMensagem = new componente.jLMensagem();
        btnPagar = new javax.swing.JButton();

        abas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                abasStateChanged(evt);
            }
        });

        jLabel1.setText("Fornecedor*:");

        jLabel2.setText("Produto*:");

        jLabel3.setText("Data de compra*:");

        txtDataDeCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDataDeCompraKeyTyped(evt);
            }
        });

        jLabel4.setText("Quantidade*:");

        txtQuantidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtQuantidadeKeyTyped(evt);
            }
        });

        jLabel7.setText("Valor unitário*:");

        txtValorUnitario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValorUnitarioKeyTyped(evt);
            }
        });

        jLabel8.setText("Data de vencimento*:");

        jLabel9.setText("Status*:");

        checkboxStatus.setText("Marque esta opção caso a conta já esteje paga.");
        checkboxStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkboxStatusActionPerformed(evt);
            }
        });

        labelModo.setText("Modo: Cadastro");

        txtDataDeVencimento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDataDeVencimentoKeyTyped(evt);
            }
        });

        btnSelecionarFornecedor.setText("Selecionar Fornecedor");
        btnSelecionarFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarFornecedorActionPerformed(evt);
            }
        });

        btnSelecionarProduto.setText("Selecionar Produto");
        btnSelecionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarProdutoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelModo)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel9))
                                .addGap(28, 28, 28)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(checkboxStatus)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(txtDataDeCompra, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtQuantidade, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtValorUnitario)
                                    .addComponent(txtDataDeVencimento, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))
                                .addGap(81, 81, 81)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnSelecionarFornecedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnSelecionarProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(34, 34, 34)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(btnSelecionarFornecedor))
                    .addComponent(txtFornecedor, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSelecionarProduto))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDataDeCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtDataDeVencimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(checkboxStatus))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                .addComponent(labelModo)
                .addContainerGap())
        );

        abas.addTab("Cadastro/Edição", jPanel1);

        tableProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableProdutos);

        jLabel6.setText("Ordem");

        comboPreco.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Status", "Data de Pagamento", "Data de vencimento"}));
        comboPreco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPrecoActionPerformed(evt);
            }
        });

        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(464, 464, 464)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboPreco, 0, 247, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultar)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConsultar)
                    .addComponent(jLabel6)
                    .addComponent(comboPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
                .addContainerGap())
        );

        abas.addTab("Consulta", jPanel2);

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnRemover.setText("Remover");
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnPagar.setText("Pagar");
        btnPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(abas)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(73, 73, 73)
                .addComponent(btnFechar)
                .addGap(33, 33, 33)
                .addComponent(btnRemover)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPagar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalvar)
                .addGap(43, 43, 43))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(abas, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSalvar)
                            .addComponent(btnEditar)
                            .addComponent(btnRemover)
                            .addComponent(btnPagar)
                            .addComponent(btnFechar))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboPrecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPrecoActionPerformed
        btnConsultarActionPerformed(evt);
    }//GEN-LAST:event_comboPrecoActionPerformed

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        popularTabela();
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        this.nome = txtProduto.getText();

        String nomeFornecedor = txtFornecedor.getText();
        String nomeProduto = txtProduto.getText();
        String dataDeCompraTexto = txtDataDeCompra.getText();
        String dataDeVencimentoTexto = txtDataDeVencimento.getText();
        String valorUnitarioTexto = txtValorUnitario.getText();
        String quantidadeTexto = txtQuantidade.getText();
        boolean status = checkboxStatus.isSelected();

        // verificações
        boolean nomeFornecedorVazio = validacao.vazio(nomeFornecedor);
        boolean nomeProdutoVazio = Validacao.vazio(nomeProduto);
        boolean dataDeCompraVazio = Validacao.vazio(dataDeCompraTexto);
        boolean dataDeVencimentoVazio = Validacao.vazio(dataDeVencimentoTexto);
        boolean valorUnitarioVazio = Validacao.vazio(valorUnitarioTexto);
        boolean quantidadeVazio = Validacao.vazio(quantidadeTexto);

        boolean dataDeCompraInvalida;
        boolean dataDeVencimentoInvalida;
        try {
            dataDeCompraInvalida = Validacao.validarDataFormatada(dataDeCompraTexto);
            dataDeVencimentoInvalida = Validacao.validarDataFormatada(dataDeVencimentoTexto);
        } catch (Exception e) {
            e.printStackTrace();
            dataDeCompraInvalida = false;
            dataDeVencimentoInvalida = false;
        }

        if (!nomeFornecedorVazio && !nomeProdutoVazio && !valorUnitarioVazio
                && !dataDeCompraVazio && !dataDeVencimentoVazio && !quantidadeVazio
                && dataDeCompraInvalida && dataDeVencimentoInvalida) {

            BigDecimal valorUnitario = BigDecimal.valueOf(Double.parseDouble(txtValorUnitario.getText()));
            BigDecimal quantidade = BigDecimal.valueOf(Double.parseDouble(txtQuantidade.getText()));

            Date dataDeCompra = new Date();
            Date dataDeVencimento = new Date();

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            try {
                dataDeCompra = formatter.parse(dataDeCompraTexto);
                dataDeVencimento = formatter.parse(dataDeVencimentoTexto);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            Fornecedorproduto fornecedorProduto = new Fornecedorproduto();
            fornecedorProduto.setValorunitario(valorUnitario);
            fornecedorProduto.setQuantidade(quantidade);
            fornecedorProduto.setStatus(status);
            fornecedorProduto.setFornecedor(this.fornecedor);
            fornecedorProduto.setProduto(this.produto);
            fornecedorProduto.setData(dataDeCompra);
            fornecedorProduto.setDatavencimento(dataDeVencimento);

            if (modo.equals("edicao")) {
                fornecedorProduto.setId(this.idFornecedorProduto);
            }

            if (dao.salvar(fornecedorProduto)) {
                mensagem("Salvo com sucesso");

                if (modo.equals("cadastro")) {
                    txtDataDeCompra.requestFocus();
                }

                limparCampos();
                popularTabela();
                pintarCamposDeBranco();

//              Reativar botões e trocar labels
                if (modo.equals("edicao")) {
                    abas.setSelectedIndex(1);
                    abas.setEnabled(true);
                    btnRemover.setEnabled(true);
                    btnEditar.setEnabled(true);
                    btnFechar.setText("Fechar");

                    trocarModo();
                    enviarParaLog("edicao");
                } else {
                    this.idFornecedorProduto = 0;
                    enviarParaLog("insercao");
                }
            } else {
                mensagem("Ocorreu um erro ao salvar produto!");
            }
        } else {
            pintarCamposDeBranco();
            mensagem("Preencha os campos corretamente");
            if (nomeFornecedorVazio) {
                txtFornecedor.setBackground(Color.PINK);
            }
            if (nomeProdutoVazio) {
                txtProduto.setBackground(Color.PINK);
            }
            if (dataDeCompraVazio) {
                txtDataDeCompra.setBackground(Color.PINK);
            }
            if (dataDeVencimentoVazio) {
                txtDataDeVencimento.setBackground(Color.PINK);
            }
            if (valorUnitarioVazio) {
                txtValorUnitario.setBackground(Color.PINK);
            }
            if (quantidadeVazio) {
                txtQuantidade.setBackground(Color.PINK);
            }
            if (!dataDeCompraInvalida) {
                txtDataDeCompra.setBackground(Color.PINK);
            }
            if (!dataDeVencimentoInvalida) {
                txtDataDeVencimento.setBackground(Color.PINK);
            }
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        trocarModo();

        // Desabilitar botões
        abas.setSelectedIndex(0);
        abas.setEnabled(false);
        btnRemover.setEnabled(false);
        btnEditar.setEnabled(false);
        btnFechar.setText("Cancelar");

        pegarIdSelecionado();

        Fornecedorproduto fornecedorProduto = (Fornecedorproduto) dao.consultaId(this.idFornecedorProduto);
        this.nome = fornecedorProduto.getProduto().getNome();
        this.fornecedor = fornecedorProduto.getFornecedor();
        this.produto = fornecedorProduto.getProduto();
        String dataDeCompra = Formatacao.ajustaDataDMA(fornecedorProduto.getData().toString());
        String dataDeVencimento = Formatacao.ajustaDataDMA(fornecedorProduto.getDatavencimento().toString());
        String quantidade = String.valueOf(fornecedorProduto.getQuantidade());
        String valorUnitario = String.valueOf(fornecedorProduto.getValorunitario());
        boolean status = fornecedorProduto.isStatus();

        nomeAnterior = nome;
        txtFornecedor.setText(this.fornecedor.getNome());
        txtProduto.setText(this.produto.getNome());
        txtDataDeCompra.setText(dataDeCompra);
        txtDataDeVencimento.setText(dataDeVencimento);
        txtQuantidade.setText(quantidade);
        txtValorUnitario.setText(valorUnitario);
        checkboxStatus.setSelected(status);

        txtDataDeCompra.requestFocus();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
        pegarIdSelecionado();

        Fornecedorproduto produto = (Fornecedorproduto) dao.consultaId(this.idFornecedorProduto);
        this.nome = produto.getProduto().getNome();

        if (dao.deletar(produto)) {
            mensagem("Removido com sucesso!");
            enviarParaLog("remocao");
            popularTabela();
        } else {
            mensagem("Erro ao remover Produto!");
        }
    }//GEN-LAST:event_btnRemoverActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        if (modo.equals("edicao")) {
            abas.setSelectedIndex(1);
            abas.setEnabled(true);
            btnRemover.setEnabled(true);
            btnFechar.setText("Fechar");

            trocarModo();
            limparCampos();
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_btnFecharActionPerformed

    private void checkboxStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkboxStatusActionPerformed

    }//GEN-LAST:event_checkboxStatusActionPerformed

    private void btnSelecionarFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarFornecedorActionPerformed
        new jdFornecedor(null, true, this).setVisible(true);
    }//GEN-LAST:event_btnSelecionarFornecedorActionPerformed

    private void btnSelecionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarProdutoActionPerformed
        new JdProdutoSelect(null, true, this).setVisible(true);
    }//GEN-LAST:event_btnSelecionarProdutoActionPerformed

    private void abasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_abasStateChanged
        if (abas.getSelectedIndex() == 0) {
            btnSalvar.setEnabled(true);
            btnRemover.setEnabled(false);
            btnEditar.setEnabled(false);
            btnPagar.setEnabled(false);
        } else {
            btnSalvar.setEnabled(false);
            btnRemover.setEnabled(true);
            btnEditar.setEnabled(true);
            btnPagar.setEnabled(true);
        }
    }//GEN-LAST:event_abasStateChanged

    private void btnPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagarActionPerformed
        pegarIdSelecionado();
        Fornecedorproduto fp = (Fornecedorproduto) dao.consultaId(this.idFornecedorProduto);
        fp.setStatus(true);
        dao.salvar(fp);
        popularTabela();
        this.nomeAnterior = fp.getProduto().getNome();
        enviarParaLog("edicao");
        mensagem("Conta paga com sucesso!");
    }//GEN-LAST:event_btnPagarActionPerformed

    private void txtDataDeCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDataDeCompraKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtDataDeCompra);
    }//GEN-LAST:event_txtDataDeCompraKeyTyped

    private void txtQuantidadeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuantidadeKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtQuantidade);
    }//GEN-LAST:event_txtQuantidadeKeyTyped

    private void txtValorUnitarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorUnitarioKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtValorUnitario);
    }//GEN-LAST:event_txtValorUnitarioKeyTyped

    private void txtDataDeVencimentoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDataDeVencimentoKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtDataDeVencimento);
    }//GEN-LAST:event_txtDataDeVencimentoKeyTyped

    private void pegarIdSelecionado() {
        int row = tableProdutos.getSelectedRow();
        this.idFornecedorProduto = (int) tableProdutos.getValueAt(row, 0);
    }

    private void pintarCamposDeBranco() {
        txtDataDeCompra.setBackground(Color.WHITE);
        txtDataDeVencimento.setBackground(Color.WHITE);
        txtFornecedor.setBackground(Color.WHITE);
        txtProduto.setBackground(Color.WHITE);
        txtQuantidade.setBackground(Color.WHITE);
        txtValorUnitario.setBackground(Color.WHITE);
    }

    public void limparCampos() {
        Formatacao.formatarData(txtDataDeCompra);
        Formatacao.formatarData(txtDataDeVencimento);
        txtFornecedor.setText("");
        txtProduto.setText("");
        txtQuantidade.setText("");
        txtValorUnitario.setText("");
    }

    public void enterParaSalvar(java.awt.event.KeyEvent evt) {
        if (evt.getKeyChar() == '\n') {
            btnSalvarActionPerformed(null);
        }
    }

    public void enviarParaLog(String evento) {
        String texto = "";
        if (evento.equals("insercao")) {
            texto = "\"\"" + this.nome + "\"\" Inserido";
        }
        if (evento.equals("edicao")) {
            texto = "\"\"" + this.nomeAnterior + "\"\" Modificado";
        }
        if (evento.equals("remocao")) {
            texto = "\"\"" + this.nome + "\"\" Removido";
        }

        Logs.escrever(this.idFornecedorProduto, this.tabela, texto, this.usuario.getLogin().getLogin());
    }

    public void digitarApenasNumeros(KeyEvent evt, JTextField txt) {
        String caracters = "0123456789.";
        String ponto = ".";
        String texto = txt.getText();

        if (texto.contains(ponto)) {
            if (evt.getKeyChar() == 46) { // 46 = code ASCII para "."
                evt.consume();
            }
        }

        if (!caracters.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }

    private void trocarModo() {
        if (modo.equals("edicao")) {
            modo = "cadastro";
            labelModo.setText("Modo: Cadastro");
        } else {
            modo = "edicao";
            labelModo.setText("Modo: Edição");
        }
    }

    private void mensagem(String texto) {
        Runnable run = new ThreadMessage(jLMensagem, texto);
        new Thread(run).start();
    }

    private void popularTabela() {
        String ordem = "";

        switch (comboPreco.getSelectedIndex()) {
            case 0:
                ordem = "status";
                break;
            case 1:
                ordem = "data";
                break;
            case 2:
                ordem = "datavencimento";
                break;
        }

        dao.listar(tableProdutos, ordem);
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
        txtProduto.setText(produto.getNome());
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
        txtFornecedor.setText(fornecedor.getNome());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane abas;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnPagar;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton btnSelecionarFornecedor;
    private javax.swing.JButton btnSelecionarProduto;
    private javax.swing.JCheckBox checkboxStatus;
    private javax.swing.JComboBox<String> comboPreco;
    private componente.jLMensagem jLMensagem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelModo;
    private javax.swing.JTable tableProdutos;
    private javax.swing.JFormattedTextField txtDataDeCompra;
    private javax.swing.JFormattedTextField txtDataDeVencimento;
    private javax.swing.JTextField txtFornecedor;
    private javax.swing.JTextField txtProduto;
    private javax.swing.JTextField txtQuantidade;
    private javax.swing.JTextField txtValorUnitario;
    // End of variables declaration//GEN-END:variables
}
