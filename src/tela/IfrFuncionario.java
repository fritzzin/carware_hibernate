/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tela;

import apoio.Encriptar;
import apoio.Formatacao;
import apoio.Logs;
import apoio.ThreadMessage;
import apoio.Validacao;
import classes_hibernate.Funcao;
import classes_hibernate.Funcionario;
import classes_hibernate.Login;
import classes_hibernate.Pessoa;
import dao_hibernate.FuncaoDAO;
import dao_hibernate.FuncionarioDAO;
import dao_hibernate.LoginDAO;
import dao_hibernate.PessoaDAO;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTextField;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class IfrFuncionario extends javax.swing.JInternalFrame {

    String modo;
    int idFuncionario;
    Validacao validacao;
    FuncionarioDAO dao;
    Pessoa pessoa;
    Funcao funcao;
    Funcionario funcionarioEdicao;

    // Variaveis para log
    Funcionario usuario;
    String nome = "";
    String nomeAnterior = "";
    String tabela = "Funcionario";

    public IfrFuncionario(Funcionario funcionario) {
        initComponents();
        this.setTitle("Cadastro de Funcionários");
        txtFuncao.setEditable(false);
        txtFuncionario.setEditable(false);

        this.usuario = funcionario;
        this.validacao = new Validacao();
        this.dao = new FuncionarioDAO();
        this.modo = "cadastro";

        abas.setSelectedIndex(1);

        popularTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        abas = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtFuncao = new javax.swing.JTextField();
        btnBuscarFuncao = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtLogin = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtSenha = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        txtCtps = new componente.JftCtps();
        jLabel5 = new javax.swing.JLabel();
        txtSalario = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtEspecialidade = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        txtDataDeAdmissao = new componente.JftData();
        jLabel8 = new javax.swing.JLabel();
        txtFuncionario = new javax.swing.JTextField();
        btnBuscarPessoa = new javax.swing.JButton();
        labelModo = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtCriterio = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableFuncionario = new javax.swing.JTable();
        btnLimparCriterio = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        comboPreco = new javax.swing.JComboBox<>();
        btnConsultar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        jLMensagem = new componente.jLMensagem();
        btnFechar = new javax.swing.JButton();
        btnRemover = new javax.swing.JButton();

        abas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                abasStateChanged(evt);
            }
        });

        jLabel1.setText("Selecione a função:*");

        btnBuscarFuncao.setText("...");
        btnBuscarFuncao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarFuncaoActionPerformed(evt);
            }
        });

        jLabel2.setText("Login:*");

        txtLogin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtLoginKeyTyped(evt);
            }
        });

        jLabel3.setText("Senha:*");

        txtSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSenhaKeyTyped(evt);
            }
        });

        jLabel4.setText("Ctps:*");

        txtCtps.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCtpsKeyTyped(evt);
            }
        });

        jLabel5.setText("Salario:*");

        txtSalario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSalarioKeyTyped(evt);
            }
        });

        jLabel6.setText("Especialidade:*");

        txtEspecialidade.setColumns(20);
        txtEspecialidade.setRows(5);
        txtEspecialidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEspecialidadeKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(txtEspecialidade);

        jLabel7.setText("Data de admissão:*");

        txtDataDeAdmissao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDataDeAdmissaoKeyTyped(evt);
            }
        });

        jLabel8.setText("Selecione o pessoa*:");

        btnBuscarPessoa.setText("...");
        btnBuscarPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPessoaActionPerformed(evt);
            }
        });

        labelModo.setText("Modo: Cadastro");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDataDeAdmissao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE)
                            .addComponent(txtFuncionario)
                            .addComponent(txtFuncao, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtLogin, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtSenha, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtCtps, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtSalario, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBuscarPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBuscarFuncao, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(labelModo))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarPessoa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarFuncao)
                    .addComponent(txtFuncao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCtps, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSalario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(txtDataDeAdmissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(labelModo)
                .addContainerGap())
        );

        abas.addTab("Cadastro/Edição", jPanel1);

        jLabel9.setText("Nome:");

        txtCriterio.setEnabled(false);

        tableFuncionario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tableFuncionario);

        btnLimparCriterio.setText("X");
        btnLimparCriterio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparCriterioActionPerformed(evt);
            }
        });

        jLabel10.setText("Nome:");

        comboPreco.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Valores padrão", "A-Z", "Z-A"}));
        comboPreco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPrecoActionPerformed(evt);
            }
        });

        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimparCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboPreco, 0, 217, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultar)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConsultar)
                    .addComponent(txtCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(comboPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLimparCriterio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        abas.addTab("Consultar", jPanel2);

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnRemover.setText("Remover");
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(abas)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(105, 105, 105)
                .addComponent(btnFechar)
                .addGap(18, 18, 18)
                .addComponent(btnRemover)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalvar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(abas)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalvar)
                        .addComponent(btnEditar)
                        .addComponent(btnRemover)
                        .addComponent(btnFechar))
                    .addComponent(jLMensagem, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarPessoaActionPerformed
        new JdPessoaSelect(null, true, this).setVisible(true);
    }//GEN-LAST:event_btnBuscarPessoaActionPerformed

    private void btnBuscarFuncaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarFuncaoActionPerformed
        new JdFuncaoSelect(null, true, this).setVisible(true);
    }//GEN-LAST:event_btnBuscarFuncaoActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        if (modo.equals("edicao")) {
            abas.setSelectedIndex(1);
            abas.setEnabled(true);
            btnRemover.setEnabled(true);
            btnFechar.setText("Fechar");

            trocarModo();
            limparCampos();
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnLimparCriterioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparCriterioActionPerformed
        txtCriterio.setText("");
        popularTabela();
    }//GEN-LAST:event_btnLimparCriterioActionPerformed

    private void comboPrecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPrecoActionPerformed
        btnConsultarActionPerformed(evt);
    }//GEN-LAST:event_comboPrecoActionPerformed

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        popularTabela();
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
        pegarIdSelecionado();

        Funcionario funcionario = (Funcionario) dao.consultaId(this.idFuncionario);
        this.nome = funcionario.getPessoa().getNome();

        if (dao.deletar(funcionario)) {
            mensagem("Removido com sucesso!");
            enviarParaLog("remocao");
            new LoginDAO().deletar(funcionario.getLogin());
            popularTabela();
        } else {
            mensagem("Erro ao remover Funcionário!");
        }
    }//GEN-LAST:event_btnRemoverActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        trocarModo();

        // Desabilitar botões
        abas.setSelectedIndex(0);
        abas.setEnabled(false);
        btnRemover.setEnabled(false);
        btnEditar.setEnabled(false);
        btnFechar.setText("Cancelar");

        pegarIdSelecionado();

        Funcionario funcionario = (Funcionario) dao.consultaId(this.idFuncionario);
        this.nome = funcionario.getPessoa().getNome();
        this.pessoa = funcionario.getPessoa();
        this.funcao = funcionario.getFuncao();
        this.funcionarioEdicao = funcionario;
        String funcao = funcionario.getFuncao().getNome();
        String login = funcionario.getLogin().getLogin();
        String senha = funcionario.getLogin().getSenha();
        String ctps = funcionario.getCtps();
        String salario = String.valueOf(funcionario.getSalario());
        String especialidade = funcionario.getEspecialidade();
        String dataDeAdmissao = String.valueOf((funcionario.getDataadmissao()));

        txtFuncionario.setText(this.nome);
        txtFuncao.setText(funcao);
        txtLogin.setText(login);
        txtSenha.setText(senha);
        txtCtps.setText(ctps);
        txtSalario.setText(salario);
        txtEspecialidade.setText(especialidade);
        txtDataDeAdmissao.setText(Formatacao.ajustaDataDMA(dataDeAdmissao));
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        this.nome = pessoa.getNome();

        String usuario = txtLogin.getText();
        String senha = String.valueOf(txtSenha.getPassword());
        senha = Encriptar.md5(senha);

        Login login = new Login();

        if (new LoginDAO().loginJaExiste(usuario) && modo.equals("cadastro")) {
            mensagem("Login existente, insira outro login");
        } else {
            if (modo.equals("cadastro")) {
                login.setLogin(usuario);
                login.setSenha(senha);
                new LoginDAO().salvar(login);
            } else {
                login = funcionarioEdicao.getLogin();
                if (!login.getLogin().equals(usuario)) {
                    login.setLogin(usuario);
                    new LoginDAO().salvar(login);
                }
//                login = (Login) new LoginDAO().consultaporLogin(usuario);
            }

            String ctps = txtCtps.getText();
            BigDecimal salario = BigDecimal.valueOf(Double.parseDouble(txtSalario.getText()));
            String especialidade = txtEspecialidade.getText();

            Date dataAdmissao;
            String data = txtDataDeAdmissao.getText();
            try {
                dataAdmissao = (Date) new SimpleDateFormat("dd/MM/yyyy").parse(data);
            } catch (ParseException ex) {
                dataAdmissao = null;
                ex.printStackTrace();
            }

            Funcionario funcionario = new Funcionario();

            if (modo.equals("edicao")) {
                funcionario.setIdpessoa(pessoa.getId());
            }

            funcionario.setFuncao(funcao);
            funcionario.setLogin(login);
            funcionario.setPessoa(pessoa);
            funcionario.setCtps(ctps);
            funcionario.setSalario(salario);
            funcionario.setEspecialidade(especialidade);
            funcionario.setDataadmissao(dataAdmissao);

            FuncionarioDAO dao = new FuncionarioDAO();

            if (dao.salvar(funcionario)) {
                mensagem("Salvo com sucesso!");

                if (modo.equals("cadastro")) {
                    enviarParaLog("insercao");
                    this.idFuncionario = 0;
                } else {
                    this.idFuncionario = 0;
                    abas.setSelectedIndex(1);
                    abas.setEnabled(true);
                    btnRemover.setEnabled(true);
                    btnEditar.setEnabled(true);
                    btnFechar.setText("Fechar");

//                    trocarModo();
                    enviarParaLog("edicao");
                }
                limparCampos();
                trocarModo();
                abas.setSelectedIndex(1);
                popularTabela();
            } else {
                mensagem("Houve um erro ao salvar!");
                if (modo.equals("cadastro")) {
                    new LoginDAO().deletar(login);
                }
            };
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void txtCtpsKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCtpsKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtCtps);
    }//GEN-LAST:event_txtCtpsKeyTyped

    private void txtSalarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSalarioKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtSalario);
    }//GEN-LAST:event_txtSalarioKeyTyped

    private void txtDataDeAdmissaoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDataDeAdmissaoKeyTyped
        enterParaSalvar(evt);
        digitarApenasNumeros(evt, txtDataDeAdmissao);
    }//GEN-LAST:event_txtDataDeAdmissaoKeyTyped

    private void txtEspecialidadeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEspecialidadeKeyTyped
        enterParaSalvar(evt);
    }//GEN-LAST:event_txtEspecialidadeKeyTyped

    private void txtSenhaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSenhaKeyTyped
        enterParaSalvar(evt);
    }//GEN-LAST:event_txtSenhaKeyTyped

    private void txtLoginKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLoginKeyTyped
        enterParaSalvar(evt);
    }//GEN-LAST:event_txtLoginKeyTyped

    private void abasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_abasStateChanged
        if (abas.getSelectedIndex() == 0) {
            btnSalvar.setEnabled(true);
            btnRemover.setEnabled(false);
            btnEditar.setEnabled(false);

        } else {
            btnSalvar.setEnabled(false);
            btnRemover.setEnabled(true);
            btnEditar.setEnabled(true);
        }
    }//GEN-LAST:event_abasStateChanged

    public void enterParaSalvar(java.awt.event.KeyEvent evt) {
        if (evt.getKeyChar() == '\n') {
            btnSalvarActionPerformed(null);
        }
    }

    public void digitarApenasNumeros(KeyEvent evt, JTextField txt) {
        String caracters = "0123456789.";
        String ponto = ".";
        String texto = txt.getText();

        if (texto.contains(ponto)) {
            if (evt.getKeyChar() == 46) { // 46 = code ASCII para "."
                evt.consume();
            }
        }

        if (!caracters.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }

    public void digitarApenasLetras(KeyEvent evt, JTextField txt) {
        String caracters = "0123456789.;/!@#$%¨&*()\'\"";

        if (!caracters.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }

    private void limparCampos() {
        txtFuncionario.setText("");
        txtFuncao.setText("");
        txtLogin.setText("");
        txtSenha.setText("");
        txtCtps.setText("");
        txtSalario.setText("");
        txtEspecialidade.setText("");
        txtDataDeAdmissao.setText("");
    }

    private void pintarCamposDeBranco() {
        txtFuncionario.setBackground(Color.WHITE);
        txtFuncao.setBackground(Color.WHITE);
        txtLogin.setBackground(Color.WHITE);
        txtSenha.setBackground(Color.WHITE);
        txtCtps.setBackground(Color.WHITE);
        txtSalario.setBackground(Color.WHITE);
        txtEspecialidade.setBackground(Color.WHITE);
        txtDataDeAdmissao.setBackground(Color.WHITE);
    }

    private void pegarIdSelecionado() {
        int row = tableFuncionario.getSelectedRow();
        this.idFuncionario = (int) tableFuncionario.getValueAt(row, 0);
    }

    private void mensagem(String texto) {
        Runnable run = new ThreadMessage(jLMensagem, texto);
        new Thread(run).start();
    }

    private void popularTabela() {
        String criterio = txtCriterio.getText();
        String ordem = "";

        switch (comboPreco.getSelectedIndex()) {
            case 0:
                ordem = "";
                break;
            case 1:
                ordem = "asc";
                break;
            case 2:
                ordem = "desc";
                break;
        }
        dao.listar(tableFuncionario, criterio, ordem);
    }

    private void trocarModo() {
        if (modo.equals("edicao")) {
            modo = "cadastro";
            labelModo.setText("Modo: Cadastro");
        } else {
            modo = "edicao";
            labelModo.setText("Modo: Edição");
        }
    }

    public void setIdFuncao(int id) {
        this.funcao = (Funcao) new FuncaoDAO().consultaId(id);
        txtFuncao.setText(this.funcao.getNome());
    }

    public void setIdPessoa(int id) {
        this.pessoa = (Pessoa) new PessoaDAO().consultaId(id);
        txtFuncionario.setText(this.pessoa.getNome());

    }

    public void enviarParaLog(String evento) {
        String texto = "";
        if (evento.equals("insercao")) {
            texto = "\"\"" + this.nome + "\"\" Inserido";
        }
        if (evento.equals("edicao")) {
            texto = "\"\"" + this.nomeAnterior + "\"\" Modificado";
        }
        if (evento.equals("remocao")) {
            texto = "\"\"" + this.nome + "\"\" Removido";
        }

        Logs.escrever(this.idFuncionario, this.tabela, texto, this.usuario.getLogin().getLogin());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane abas;
    private javax.swing.JButton btnBuscarFuncao;
    private javax.swing.JButton btnBuscarPessoa;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnLimparCriterio;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> comboPreco;
    private componente.jLMensagem jLMensagem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelModo;
    private javax.swing.JTable tableFuncionario;
    private javax.swing.JTextField txtCriterio;
    private componente.JftCtps txtCtps;
    private componente.JftData txtDataDeAdmissao;
    private javax.swing.JTextArea txtEspecialidade;
    private javax.swing.JTextField txtFuncao;
    private javax.swing.JTextField txtFuncionario;
    private javax.swing.JTextField txtLogin;
    private javax.swing.JTextField txtSalario;
    private javax.swing.JPasswordField txtSenha;
    // End of variables declaration//GEN-END:variables
}
