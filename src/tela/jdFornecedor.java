package tela;

import classes_hibernate.Fornecedor;
import dao_hibernate.FornecedorDAO;

/**
 *
 * @author Augusto Fritz
 */
public class jdFornecedor extends javax.swing.JDialog {

    FornecedorDAO dao;
    IfrFornecedorProduto ifrFornecedorProduto;

    public jdFornecedor(java.awt.Frame parent, boolean modal, IfrFornecedorProduto ifrFornecedorProduto) {
        super(parent, modal);
        initComponents();

        dao = new FornecedorDAO();
        this.ifrFornecedorProduto = ifrFornecedorProduto;

        popularTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaFornecedor = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtCriterio = new javax.swing.JTextField();
        btnConsultar = new javax.swing.JButton();
        btnSelecionar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        btnLimparCriterio = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        comboOrdem = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tabelaFornecedor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelaFornecedor);

        jLabel1.setText("Nome:");

        btnConsultar.setText("Consultar");

        btnSelecionar.setText("Selecionar");
        btnSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarActionPerformed(evt);
            }
        });

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnLimparCriterio.setText("X");
        btnLimparCriterio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparCriterioActionPerformed(evt);
            }
        });

        jLabel2.setText("Ordem:");

        comboOrdem.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Padrão", "A-Z", "Z-A"}));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 870, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCriterio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimparCriterio)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboOrdem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnConsultar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnFechar)
                        .addGap(18, 18, 18)
                        .addComponent(btnSelecionar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConsultar)
                    .addComponent(btnLimparCriterio)
                    .addComponent(jLabel2)
                    .addComponent(comboOrdem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSelecionar)
                    .addComponent(btnFechar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnLimparCriterioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparCriterioActionPerformed
        txtCriterio.setText("");
        popularTabela();
    }//GEN-LAST:event_btnLimparCriterioActionPerformed

    private void btnSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarActionPerformed
        int row = tabelaFornecedor.getSelectedRow();
        int idFornecedor = (int) tabelaFornecedor.getValueAt(row, 0);
        Fornecedor fornecedor = (Fornecedor) dao.consultaId(idFornecedor);
        ifrFornecedorProduto.setFornecedor(fornecedor);
        this.dispose();
    }//GEN-LAST:event_btnSelecionarActionPerformed

    private void popularTabela() {
        String criterio = txtCriterio.getText();
        String ordem = "";

        switch (comboOrdem.getSelectedIndex()) {
            case 0:
                ordem = "";
                break;
            case 1:
                ordem = "desc";
                break;
            case 2:
                ordem = "asc";
                break;
        }
        dao.listar(tabelaFornecedor, criterio, ordem);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnLimparCriterio;
    private javax.swing.JButton btnSelecionar;
    private javax.swing.JComboBox<String> comboOrdem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaFornecedor;
    private javax.swing.JTextField txtCriterio;
    // End of variables declaration//GEN-END:variables
}
