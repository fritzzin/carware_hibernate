/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tela;

import apoio.Logs;
import apoio.ThreadMessage;
import classes_hibernate.Funcionario;
import classes_hibernate.Os;
import classes_hibernate.Produto;
import classes_hibernate.Produtoos;
import dao_hibernate.OsDAO;
import dao_hibernate.ProdutoDAO;
import dao_hibernate.ProdutoosDAO;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author lucas.assis
 */
public class JdProdutoSelect extends javax.swing.JDialog {

    ProdutoDAO dao;

    IfrOrcamentoOS ifrorc = null;
    IfrOS ifr = null;
    IfrFornecedorProduto ifrFornecedorProduto = null;

    Funcionario usuario;
    String nome = "";
    String tabela = "ProdutoOs";
    int idProdutoOs = 0;

    /**
     * Creates new form JdProdutoSelect
     */
    public JdProdutoSelect(java.awt.Frame parent, boolean modal, IfrOrcamentoOS ifros, Funcionario f) {
        super(parent, modal);
        initComponents();
        this.usuario = f;
        dao = new ProdutoDAO();
        this.ifrorc = ifros;
        popularTabela();
        this.setTitle("Selecionar produto");
        btnSelecionarProdutoFornecedor.setVisible(false);
    }

    public JdProdutoSelect(java.awt.Frame parent, boolean modal, IfrOS ifros, Funcionario f) {
        super(parent, modal);
        initComponents();
        this.usuario = f;
        dao = new ProdutoDAO();
        this.ifr = ifros;
        popularTabela();
        this.setTitle("Selecionar produto");
        btnSelecionarProdutoFornecedor.setVisible(false);
    }

    public JdProdutoSelect(java.awt.Frame parent, boolean modal, IfrFornecedorProduto ifrFornecedorProduto) {
        super(parent, modal);
        initComponents();
        this.setTitle("Selecionar produto");

        this.dao = new ProdutoDAO();
        this.ifrFornecedorProduto = ifrFornecedorProduto;

        btnSelecionarProduto.setVisible(false);
        btnSelecionarProdutoFornecedor.setVisible(true);

        jTQuantidade.setEnabled(false);

        popularTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        abas = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtConsultaNome = new javax.swing.JTextField();
        btnConsultar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        comboPreco = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableProdutos = new javax.swing.JTable();
        btnLimparCriterio = new javax.swing.JButton();
        jTQuantidade = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnSelecionarProduto = new javax.swing.JButton();
        jBFechar = new javax.swing.JButton();
        jLMensagem = new componente.jLMensagem();
        btnSelecionarProdutoFornecedor = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        abas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                abasStateChanged(evt);
            }
        });

        jLabel5.setText("Nome:");

        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        jLabel6.setText("Valor Venda:");

        comboPreco.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Valores padrão", "Maior para menor", "Menor para maior"}));
        comboPreco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPrecoActionPerformed(evt);
            }
        });

        tableProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableProdutos);

        btnLimparCriterio.setText("X");
        btnLimparCriterio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparCriterioActionPerformed(evt);
            }
        });

        jTQuantidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTQuantidadeKeyTyped(evt);
            }
        });

        jLabel1.setText("Quantidade:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtConsultaNome, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimparCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboPreco, 0, 239, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultar))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConsultar)
                    .addComponent(txtConsultaNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(comboPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLimparCriterio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        abas.addTab("Consulta", jPanel2);

        getContentPane().add(abas, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 2, 895, 424));

        btnSelecionarProduto.setText("Selecionar");
        btnSelecionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarProdutoActionPerformed(evt);
            }
        });
        getContentPane().add(btnSelecionarProduto, new org.netbeans.lib.awtextra.AbsoluteConstraints(809, 431, -1, -1));

        jBFechar.setText("Fechar");
        jBFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFecharActionPerformed(evt);
            }
        });
        getContentPane().add(jBFechar, new org.netbeans.lib.awtextra.AbsoluteConstraints(721, 431, -1, -1));
        getContentPane().add(jLMensagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 431, 560, 33));

        btnSelecionarProdutoFornecedor.setText("Selecionar");
        btnSelecionarProdutoFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarProdutoFornecedorActionPerformed(evt);
            }
        });
        getContentPane().add(btnSelecionarProdutoFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 430, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        popularTabela();
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void comboPrecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPrecoActionPerformed
        btnConsultarActionPerformed(evt);
    }//GEN-LAST:event_comboPrecoActionPerformed

    private void btnLimparCriterioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparCriterioActionPerformed
        txtConsultaNome.setText("");
        popularTabela();
    }//GEN-LAST:event_btnLimparCriterioActionPerformed

    private void abasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_abasStateChanged

    }//GEN-LAST:event_abasStateChanged

    private void jBFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFecharActionPerformed
        // TODO add your handling code here:
        try {
            if (ifrFornecedorProduto != null) {
                this.dispose();
            }
            if (ifrorc != null) {
                ifrorc.atualizaValores();
            } else {
                ifr.atualizaValores();
            }
            this.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jBFecharActionPerformed

    private void btnSelecionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarProdutoActionPerformed
        // TODO add your handling code here:
        int idos = 0;

        if (ifrorc != null) {
            idos = ifrorc.getIDOS();
        } else {
            idos = ifr.getIDOS();
        }

        int idProd = 0;
        int qtd = 0;
        BigDecimal prodValue;
        if (tableProdutos.getSelectedRow() == -1) {
            mensagem("Favor selecionar uma linha!");
        } else {
            idProd = Integer.parseInt(tableProdutos.getValueAt(tableProdutos.getSelectedRow(), 0).toString());
            Produtoos pdOS = new Produtoos();
            Produto p = (Produto) new ProdutoDAO().consultaId(idProd);
            pdOS.setProduto(p);
            idProd = p.getId();
            prodValue = p.getValorvenda();
            Date d = new Date();
            if (jTQuantidade.getText() != "") {
                qtd = Integer.parseInt(jTQuantidade.getText());
                pdOS.setData(d);
                pdOS.setValorunitario(prodValue);
                pdOS.setOs((Os) new OsDAO().consultaId(idos));
                pdOS.setQuantidade(qtd);
                qtd = qtd * -1;
                p.setQuantidade((p.getQuantidade().add(new BigDecimal(qtd))));
                if (new ProdutoosDAO().salvar(pdOS)) {
                    mensagem("Produto inserido!");
                    new ProdutoosDAO().salvar(p);
                    //insere valores no produtoos
                    nome = "";
                    idProdutoOs = pdOS.getId();
                    enviarParaLog("insercao");

                    //modifica os registros de produto **qtd
                    nome = p.getNome();
                    tabela = "Produto";
                    idProdutoOs = p.getId();
                    enviarParaLog("edicao");
                    popularTabela();
                }
            } else {
                mensagem("Insira a quantidade do produto!");
            }
        }
    }//GEN-LAST:event_btnSelecionarProdutoActionPerformed

    private void jTQuantidadeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTQuantidadeKeyTyped
        // TODO add your handling code here:
        digitarApenasNumeros(evt);
    }//GEN-LAST:event_jTQuantidadeKeyTyped

    private void btnSelecionarProdutoFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarProdutoFornecedorActionPerformed
        if (tableProdutos.getSelectedRow() == -1) {
            mensagem("Favor selecionar uma linha!");
        } else {
            int row = tableProdutos.getSelectedRow();
            int idFornecedorProduto = (int) tableProdutos.getValueAt(row, 0);
            Produto produto = (Produto) dao.consultaId(idFornecedorProduto);
            ifrFornecedorProduto.setProduto(produto);
            this.dispose();
        }
    }//GEN-LAST:event_btnSelecionarProdutoFornecedorActionPerformed

    public void mensagem(String texto) {
        Runnable run = new ThreadMessage(jLMensagem, texto);
        new Thread(run).start();
    }

    public void enviarParaLog(String evento) {
        String texto = "";
        if (evento.equals("insercao")) {
            texto = "\"\"" + this.nome + "\"\" Inserido";
        }
        if (evento.equals("edicao")) {
            texto = "\"\"" + this.nome + "\"\" Modificado";
        }
        if (evento.equals("remocao")) {
            texto = "\"\"" + this.nome + "\"\" Removido";
        }

        Logs.escrever(this.idProdutoOs, this.tabela, texto, this.usuario.getLogin().getLogin());
    }

    public void digitarApenasNumeros(KeyEvent evt) {
        String caracters = "0123456789";

        if (!caracters.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }

    private void popularTabela() {
        String criterio = txtConsultaNome.getText();
        String ordem = "";

        switch (comboPreco.getSelectedIndex()) {
            case 0:
                ordem = "";
                break;
            case 1:
                ordem = "desc";
                break;
            case 2:
                ordem = "asc";
                break;
        }

        dao.listar(tableProdutos, criterio, ordem);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane abas;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnLimparCriterio;
    private javax.swing.JButton btnSelecionarProduto;
    private javax.swing.JButton btnSelecionarProdutoFornecedor;
    private javax.swing.JComboBox<String> comboPreco;
    private javax.swing.JButton jBFechar;
    private componente.jLMensagem jLMensagem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTQuantidade;
    private javax.swing.JTable tableProdutos;
    private javax.swing.JTextField txtConsultaNome;
    // End of variables declaration//GEN-END:variables
}
