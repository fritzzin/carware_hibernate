/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tela;

import apoio.Logs;
import apoio.ThreadMessage;
import classes.Marca;
import classes_hibernate.Funcionario;
import dao.GenericDAO;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class JdMarcaSelect extends javax.swing.JDialog {

    JdModeloSelect jdms = null;
    Funcionario usuario;
    String nome = "";
    String nomeAnterior = "";
    String tabela = "Marca";
    int idMarca = 0;


    /**
     * Creates new form JdMarcaSelect
     */
    public JdMarcaSelect(java.awt.Frame parent, boolean modal, JdModeloSelect jdms, Funcionario f) {
        super(parent, modal);
        this.setTitle("Cadastro de Marcas");
        initComponents();
        this.usuario = f;
        jTabbedPane1.setEnabledAt(2, false);
        jTabbedPane1.setSelectedIndex(1);
        jBSalvar.setEnabled(false);
        this.jdms = jdms;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTMarca = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jIfrConsultaMarca = new componente.jIfrConsultaMarca();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTMarcaUpdate = new javax.swing.JTextField();
        jBSelecionar = new javax.swing.JButton();
        jBSalvar = new javax.swing.JButton();
        jBEditar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jLMensagem = new componente.jLMensagem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        jLabel1.setText("Nome*:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTMarca, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(217, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro", jPanel1);

        jIfrConsultaMarca.setVisible(true);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jIfrConsultaMarca, javax.swing.GroupLayout.DEFAULT_SIZE, 525, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jIfrConsultaMarca, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Seleção", jPanel2);

        jLabel2.setText("Nome:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTMarcaUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(250, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTMarcaUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(180, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Editar", jPanel3);

        jBSelecionar.setText("Selecionar");
        jBSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSelecionarActionPerformed(evt);
            }
        });

        jBSalvar.setText("Salvar");
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });

        jBEditar.setText("Editar");
        jBEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBEditarActionPerformed(evt);
            }
        });

        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBExcluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBEditar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBSalvar)
                .addGap(18, 18, 18)
                .addComponent(jBSelecionar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jBSelecionar)
                        .addComponent(jBSalvar)
                        .addComponent(jBEditar)
                        .addComponent(jBExcluir)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        // TODO add your handling code here:
        classes.Marca m = new classes.Marca();
        if (jTabbedPane1.getSelectedIndex() == 2) {
            m.setId(jIfrConsultaMarca.getIdCampoSelecionado());
            m.setNome(jTMarcaUpdate.getText());
        } else {
            m.setNome(jTMarca.getText());
        }
        int id = GenericDAO.saveReturnID(m);
        if (id != -1) {
            Runnable run = new ThreadMessage(jLMensagem, "Salvo!");
            new Thread(run).start();
            if (jTabbedPane1.getSelectedIndex() == 2) {
                nome = m.getNome();
                idMarca = id;
                enviarParaLog("edicao");
            } else {
                nome = m.getNome();
                idMarca = id;
                enviarParaLog("insercao");
            }
        } else {
            Runnable run = new ThreadMessage(jLMensagem, "Erro!");
            new Thread(run).start();
        }
        jIfrConsultaMarca.atualizaTable();

    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSelecionarActionPerformed
        // TODO add your handling code here:
        int id = jIfrConsultaMarca.getIdCampoSelecionado();
        Marca m = GenericDAO.consultaMarcaID(id);
        jdms.setMarca(m);
        this.dispose();

    }//GEN-LAST:event_jBSelecionarActionPerformed

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        // TODO add your handling code here:
        switch (jTabbedPane1.getSelectedIndex()) {
            case 0:
                jBEditar.setEnabled(false);
                jBExcluir.setEnabled(false);
                jBSelecionar.setEnabled(false);
                jBSalvar.setEnabled(true);
                ;
                break;
            case 1:
                jBEditar.setEnabled(true);
                jBExcluir.setEnabled(true);
                jBSelecionar.setEnabled(true);
                jBSalvar.setEnabled(false);
                ;
                break;
            case 2:
                jBEditar.setEnabled(false);
                jBExcluir.setEnabled(false);
                jBSelecionar.setEnabled(false);
                jBSalvar.setEnabled(true);
                ;
                break;
        }
    }//GEN-LAST:event_jTabbedPane1MouseClicked

    private void jBEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBEditarActionPerformed
        // TODO add your handling code here:
        jTMarcaUpdate.setText(jIfrConsultaMarca.getNomeCampoSelecionado());
        jTabbedPane1.setSelectedIndex(2);
        jTabbedPane1MouseClicked(null);
        this.nomeAnterior = jTMarcaUpdate.getText();
    }//GEN-LAST:event_jBEditarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        // TODO add your handling code here:
        Marca m = new Marca();
        m.setId(jIfrConsultaMarca.getIdCampoSelecionado());
        m.setNome(jIfrConsultaMarca.getNomeCampoSelecionado());
        this.nome = m.getNome();
        if(GenericDAO.remove(m)){
            enviarParaLog("remocao");
        }
        jIfrConsultaMarca.atualizaTable();
    }//GEN-LAST:event_jBExcluirActionPerformed

    public void enviarParaLog(String evento) {
        String texto = "";
        if (evento.equals("insercao")) {
            texto = "\"\"" + this.nome + "\"\" Inserido";
        }
        if (evento.equals("edicao")) {
            texto = "\"\"" + this.nomeAnterior + "\"\" Modificado";
        }
        if (evento.equals("remocao")) {
            texto = "\"\"" + this.nome + "\"\" Removido";
        }

        Logs.escrever(this.idMarca, this.tabela, texto, this.usuario.getLogin().getLogin());
    }
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBEditar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBSalvar;
    private javax.swing.JButton jBSelecionar;
    private componente.jIfrConsultaMarca jIfrConsultaMarca;
    private componente.jLMensagem jLMensagem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField jTMarca;
    private javax.swing.JTextField jTMarcaUpdate;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}
