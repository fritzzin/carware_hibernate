package tela;

import apoio.ArrayMap;
import apoio.ConexaoBD;
import classes_hibernate.Fornecedorproduto;
import classes_hibernate.Funcionario;
import classes_hibernate.Os;
import dao_hibernate.FornecedorProdutoDAO;
import dao_hibernate.OsDAO;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class FrmMenu extends javax.swing.JFrame {

    Funcionario funcionario = null;
    String loginUsuario;

    // Variáveis para relatórios
    int idPessoa = -1;
    int idOS = -1;
    Date dataInicio;
    Date dataFim;

    /**
     *
     * @param f
     */
    public FrmMenu(Funcionario f) {
        initComponents();
        this.funcionario = f;
        IfrLogin ifr = new IfrLogin(f.getPessoa().getNome(), f.getFuncao().getNome(), this);
        jDesktopPane1.add(ifr);
        ifr.setVisible(true);
        this.loginUsuario = funcionario.getLogin().getLogin();
        this.setLocationRelativeTo(null);
        this.setExtendedState(MAXIMIZED_BOTH);
        this.requestFocus();

        this.setTitle("CARWARE - Menu Principal");
        verificarPermissoes();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuCadastros = new javax.swing.JMenu();
        itemPessoas = new javax.swing.JMenuItem();
        itemVeiculos = new javax.swing.JMenuItem();
        itemProdutos = new javax.swing.JMenuItem();
        itemFuncionarios = new javax.swing.JMenuItem();
        itemFornecedores = new javax.swing.JMenuItem();
        menuOS = new javax.swing.JMenu();
        itemAberturaOS = new javax.swing.JMenuItem();
        itemOrcamentos = new javax.swing.JMenuItem();
        itemOSAbertas = new javax.swing.JMenuItem();
        itemEntregas = new javax.swing.JMenuItem();
        menuFinanceiro = new javax.swing.JMenu();
        itemContasEmAberto = new javax.swing.JMenuItem();
        menuRelatorios = new javax.swing.JMenu();
        menuRelatorioDeOS = new javax.swing.JMenu();
        itemRelatorioOSEmAberto = new javax.swing.JMenuItem();
        itemRelatorioOSEspecifica = new javax.swing.JMenuItem();
        itemRelatorioOSEntreDatas = new javax.swing.JMenuItem();
        itemRelatorioOSPorCliente = new javax.swing.JMenuItem();
        itemRelatorioDeEstoque = new javax.swing.JMenuItem();
        itemRelatorioDeContas = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        menuCadastros.setText("Cadastros");

        itemPessoas.setText("Pessoa");
        itemPessoas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemPessoasActionPerformed(evt);
            }
        });
        menuCadastros.add(itemPessoas);

        itemVeiculos.setText("Veículo");
        itemVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemVeiculosActionPerformed(evt);
            }
        });
        menuCadastros.add(itemVeiculos);

        itemProdutos.setText("Produto");
        itemProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemProdutosActionPerformed(evt);
            }
        });
        menuCadastros.add(itemProdutos);

        itemFuncionarios.setText("Funcionário");
        itemFuncionarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemFuncionariosActionPerformed(evt);
            }
        });
        menuCadastros.add(itemFuncionarios);

        itemFornecedores.setText("Fornecedor");
        itemFornecedores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemFornecedoresActionPerformed(evt);
            }
        });
        menuCadastros.add(itemFornecedores);

        jMenuBar1.add(menuCadastros);

        menuOS.setText("OS");

        itemAberturaOS.setText("Abrir OS");
        itemAberturaOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemAberturaOSActionPerformed(evt);
            }
        });
        menuOS.add(itemAberturaOS);

        itemOrcamentos.setText("Orcamentos");
        itemOrcamentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemOrcamentosActionPerformed(evt);
            }
        });
        menuOS.add(itemOrcamentos);

        itemOSAbertas.setText("Os abertas");
        itemOSAbertas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemOSAbertasActionPerformed(evt);
            }
        });
        menuOS.add(itemOSAbertas);

        itemEntregas.setText("Entregas");
        itemEntregas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemEntregasActionPerformed(evt);
            }
        });
        menuOS.add(itemEntregas);

        jMenuBar1.add(menuOS);

        menuFinanceiro.setText("Financeiro");

        itemContasEmAberto.setText("Contas em aberto");
        itemContasEmAberto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemContasEmAbertoActionPerformed(evt);
            }
        });
        menuFinanceiro.add(itemContasEmAberto);

        jMenuBar1.add(menuFinanceiro);

        menuRelatorios.setText("Relatórios");

        menuRelatorioDeOS.setText("Ordem de Serviço");

        itemRelatorioOSEmAberto.setText("Em aberto");
        itemRelatorioOSEmAberto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRelatorioOSEmAbertoActionPerformed(evt);
            }
        });
        menuRelatorioDeOS.add(itemRelatorioOSEmAberto);

        itemRelatorioOSEspecifica.setText("Específica");
        itemRelatorioOSEspecifica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRelatorioOSEspecificaActionPerformed(evt);
            }
        });
        menuRelatorioDeOS.add(itemRelatorioOSEspecifica);

        itemRelatorioOSEntreDatas.setText("Entre datas");
        itemRelatorioOSEntreDatas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRelatorioOSEntreDatasActionPerformed(evt);
            }
        });
        menuRelatorioDeOS.add(itemRelatorioOSEntreDatas);

        itemRelatorioOSPorCliente.setText("Por cliente");
        itemRelatorioOSPorCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRelatorioOSPorClienteActionPerformed(evt);
            }
        });
        menuRelatorioDeOS.add(itemRelatorioOSPorCliente);

        menuRelatorios.add(menuRelatorioDeOS);

        itemRelatorioDeEstoque.setText("Relatório de estoque");
        itemRelatorioDeEstoque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRelatorioDeEstoqueActionPerformed(evt);
            }
        });
        menuRelatorios.add(itemRelatorioDeEstoque);

        itemRelatorioDeContas.setText("Relatório de contas");
        itemRelatorioDeContas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRelatorioDeContasActionPerformed(evt);
            }
        });
        menuRelatorios.add(itemRelatorioDeContas);

        jMenuBar1.add(menuRelatorios);

        jMenu1.setText("Graficos");

        jMenuItem5.setText("Os status");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuItem1.setText("Contas status");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        menuAjuda.setText("Ajuda");
        menuAjuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menuAjudaMouseClicked(evt);
            }
        });
        jMenuBar1.add(menuAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 686, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void itemProdutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemProdutosActionPerformed
        if (!verificarJanela("Cadastro de Produtos")) {
            IfrProduto tela = new IfrProduto(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemProdutosActionPerformed

    private void itemFornecedoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemFornecedoresActionPerformed
        if (!verificarJanela("Cadastro de Fornecedores")) {
            IfrFornecedor tela = new IfrFornecedor(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemFornecedoresActionPerformed

    private void itemPessoasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemPessoasActionPerformed
        if (!verificarJanela("Cadastro de Pessoas")) {
            IfrPessoa tela = new IfrPessoa(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemPessoasActionPerformed

    private void itemFuncionariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemFuncionariosActionPerformed
        if (!verificarJanela("Cadastro de Funcionários")) {
            IfrFuncionario tela = new IfrFuncionario(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemFuncionariosActionPerformed

    private void itemVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemVeiculosActionPerformed
        if (!verificarJanela("Cadastro de Veículos")) {
            IfrVeiculos tela = new IfrVeiculos(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemVeiculosActionPerformed

    private void menuAjudaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuAjudaMouseClicked
        JOptionPane.showMessageDialog(null, "Para esclarecer duvidas entre em contato através do email e telefone abaixo:\n"
                + "Email: carwaresup@doom.com\n"
                + "Telefone: (51)99999-9999");
    }//GEN-LAST:event_menuAjudaMouseClicked

    private void itemAberturaOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemAberturaOSActionPerformed
        if (!verificarJanela("Ordem de Serviço")) {
            IfrAberturaOS tela = new IfrAberturaOS(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemAberturaOSActionPerformed

    private void itemOrcamentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemOrcamentosActionPerformed
        if (!verificarJanela("Alteração/Finalização de OS")) {
            IfrOrcamentoOS tela = new IfrOrcamentoOS(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemOrcamentosActionPerformed

    private void itemOSAbertasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemOSAbertasActionPerformed
        // TODO add your handling code here:
        if (!verificarJanela("Ordem de servico")) {
            IfrOS tela = new IfrOS(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemOSAbertasActionPerformed

    private void itemRelatorioDeEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRelatorioDeEstoqueActionPerformed
        relatorioSemParamentro("Estoque");
    }//GEN-LAST:event_itemRelatorioDeEstoqueActionPerformed

    private void itemContasEmAbertoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemContasEmAbertoActionPerformed
        if (!verificarJanela("Contas")) {
            IfrFornecedorProduto tela = new IfrFornecedorProduto(funcionario);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemContasEmAbertoActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        ArrayList<Os> oss = (ArrayList<Os>) new OsDAO().listaOsGrafico();
        double statusOrcamento = 0;
        double statusOs = 0;
        double statusEntrega = 0;

        ArrayList<ArrayMap> array = new ArrayList<>();
        for (int i = 0; i < oss.size(); i++) {
            if (oss.get(i).getStatus() == 'A') {
                statusOrcamento++;
            }
            if (oss.get(i).getStatus() == 'B') {
                statusOs++;
            }
            if (oss.get(i).getStatus() == 'C') {
                statusEntrega++;
            }
        }
        double total = statusOs + statusOrcamento + statusEntrega;

        array.add(new ArrayMap("Orcamento (" + (int) statusOrcamento + ")", (statusOrcamento / total) * 100));
        array.add(new ArrayMap("Em servico (" + (int) statusOs + ")", (statusOs / total) * 100));
        array.add(new ArrayMap("Aguardando entrega para Cliente (" + (int) statusEntrega + ")", (statusEntrega / total) * 100));
        IfrGrafico ifrg = new IfrGrafico("OS status", "OS Status", array);
        jDesktopPane1.add(ifrg);
        ifrg.setVisible(true);


    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void itemRelatorioDeContasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRelatorioDeContasActionPerformed
        relatorioSemParamentro("ContasAPagar");
    }//GEN-LAST:event_itemRelatorioDeContasActionPerformed

    private void itemRelatorioOSEmAbertoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRelatorioOSEmAbertoActionPerformed
        relatorioSemParamentro("OSEmAberto");
    }//GEN-LAST:event_itemRelatorioOSEmAbertoActionPerformed

    private void itemRelatorioOSPorClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRelatorioOSPorClienteActionPerformed
        new JdPessoaSelect(this, true, this).setVisible(true);
        Map parametros = new HashMap();
        parametros.put("idCliente", this.idPessoa);

        relatorioComParametro("OSPorCliente", parametros);
    }//GEN-LAST:event_itemRelatorioOSPorClienteActionPerformed

    private void itemRelatorioOSEntreDatasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRelatorioOSEntreDatasActionPerformed
        new JdSelecaoEntreDatas(this, true, this).setVisible(true);

        Map parametros = new HashMap();
        parametros.put("dataInicio", this.dataInicio);
        parametros.put("dataFim", this.dataFim);

        relatorioComParametro("OSEntreDatas", parametros);
    }//GEN-LAST:event_itemRelatorioOSEntreDatasActionPerformed

    private void itemRelatorioOSEspecificaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRelatorioOSEspecificaActionPerformed
        new JdSelecaoDeOS(this, rootPaneCheckingEnabled, this).setVisible(true);

        Map parametros = new HashMap();
        parametros.put("idOS", this.idOS);

        relatorioComParametro("OSEspecifica", parametros);
    }//GEN-LAST:event_itemRelatorioOSEspecificaActionPerformed

    private void itemEntregasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemEntregasActionPerformed
        // TODO add your handling code here:
        if (!verificarJanela("Ordem de servico")) {
            IfrEntregaOS tela = new IfrEntregaOS(funcionario, this);
            jDesktopPane1.add(tela);
            tela.setVisible(true);
        }
    }//GEN-LAST:event_itemEntregasActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        ArrayList<Fornecedorproduto> fp = (ArrayList<Fornecedorproduto>) new FornecedorProdutoDAO().listar();
        double Ativas = 0;
        double Inativas = 0;
        double Total = 0;

        ArrayList<ArrayMap> array = new ArrayList<>();

        for (int i = 0; i < fp.size(); i++) {
            Total++;
            if (fp.get(i).isStatus()) {
                Ativas++;
            } else {
                Inativas++;
            }
        }

        array.add(new ArrayMap("Ativas (" + (int) Ativas + ")", (Ativas / Total) * 100));
        array.add(new ArrayMap("Inativas (" + (int) Inativas + ")", (Inativas / Total) * 100));
        IfrGrafico ifrg = new IfrGrafico("Contas Status", "Contas a pagar", array);
        jDesktopPane1.add(ifrg);
        ifrg.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    public boolean verificarJanela(String titulo) {
        JInternalFrame[] abertas = jDesktopPane1.getAllFrames();

        for (JInternalFrame aberta : abertas) {
            if (aberta.getTitle().equals(titulo)) {
                System.out.println("Já existe uma janela \"" + titulo + "\" aberta!");
                return true;
            }
        }
        return false;
    }

    public void relatorioSemParamentro(String nomeDoRelatorio) {
        String caminho = "/relatorios/" + nomeDoRelatorio + ".jrxml";

        try {
            // Compila o relatorio
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream(caminho));

            // Mapeia campos de parametros para o relatorio, mesmo que nao existam
            Map parametros = new HashMap();

            // Executa relatoio
            JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, ConexaoBD.getInstance().getConnection());

            // Exibe resultado em video
            JasperViewer.viewReport(impressao, false);

            gerarPDF(nomeDoRelatorio, impressao);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao gerar relatório: " + e);
        }
    }

    public void relatorioComParametro(String nomeDoRelatorio, Map parametros) {
        String caminho = "/relatorios/" + nomeDoRelatorio + ".jrxml";
        try {
//            // Compila o relatorio
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream(caminho));

//            // Executa relatoio
            JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, ConexaoBD.getInstance().getConnection());

//            // Exibe resultado em video
            JasperViewer.viewReport(impressao, false);

            gerarPDF(nomeDoRelatorio, impressao);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao gerar relatório: " + e);
        }
    }

    public void gerarPDF(String nomeDoRelatorio, JasperPrint impressao) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH_mm_ss");
        Date hora = Calendar.getInstance().getTime();
        String horaAtualFormatada = sdf.format(hora);

        try {
            // nome do arquivo PDF salvo na pasta /src/relatorios/pdf
            OutputStream saida = new FileOutputStream("pdf/" + nomeDoRelatorio + "_" + java.time.LocalDate.now() + "_" + horaAtualFormatada + ".pdf");
            // exporta para pdf
            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, impressao);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, saida);

            exporter.exportReport();
        } catch (Exception e) {
            throw new RuntimeException("Erro ao gerar relatório", e);
        }
    }

    private void verificarPermissoes() {

        if (funcionario.getFuncao().getNome().equals("Mecanico")) {
            menuCadastros.setVisible(false);
            itemAberturaOS.setVisible(false);
            menuFinanceiro.setVisible(false);
            menuRelatorios.setVisible(false);
        }
        if (funcionario.getFuncao().getNome().equals("Atendente")) {
            itemFuncionarios.setVisible(false);
            itemFornecedores.setVisible(false);
            itemOrcamentos.setVisible(false);
            itemOSAbertas.setVisible(false);
            menuFinanceiro.setVisible(false);
            itemRelatorioDeContas.setVisible(false);
            itemRelatorioDeEstoque.setVisible(false);
            itemRelatorioOSEmAberto.setVisible(false);
            itemRelatorioOSEntreDatas.setVisible(false);
            itemRelatorioOSPorCliente.setVisible(false);
        }
        if (funcionario.getFuncao().getNome().equals("Estoque")) {
            itemPessoas.setVisible(false);
            itemVeiculos.setVisible(false);
            itemFuncionarios.setVisible(false);
            menuOS.setVisible(false);
            menuFinanceiro.setVisible(false);
            itemRelatorioDeContas.setVisible(false);
            menuRelatorioDeOS.setVisible(false);
        }
        if (funcionario.getFuncao().getNome().equals("Gerente")) {

        }
    }

    public void gerarRelatorioDeOsEspecifica(int id) {
        Map parametros = new HashMap();
        parametros.put("idOS", id);

        relatorioComParametro("OSEspecifica", parametros);
    }

    public void setIdPessoa(int id) {
        this.idPessoa = id;
    }

    public void setDatas(Date inicio, Date fim) {
        this.dataInicio = inicio;
        this.dataFim = fim;
    }

    public void setIdOS(int id) {
        this.idOS = id;
    }

    public void logout() {
        DlgLogin dlg = new DlgLogin(null, true);
        this.dispose();
        dlg.setVisible(true);

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem itemAberturaOS;
    private javax.swing.JMenuItem itemContasEmAberto;
    private javax.swing.JMenuItem itemEntregas;
    private javax.swing.JMenuItem itemFornecedores;
    private javax.swing.JMenuItem itemFuncionarios;
    private javax.swing.JMenuItem itemOSAbertas;
    private javax.swing.JMenuItem itemOrcamentos;
    private javax.swing.JMenuItem itemPessoas;
    private javax.swing.JMenuItem itemProdutos;
    private javax.swing.JMenuItem itemRelatorioDeContas;
    private javax.swing.JMenuItem itemRelatorioDeEstoque;
    private javax.swing.JMenuItem itemRelatorioOSEmAberto;
    private javax.swing.JMenuItem itemRelatorioOSEntreDatas;
    private javax.swing.JMenuItem itemRelatorioOSEspecifica;
    private javax.swing.JMenuItem itemRelatorioOSPorCliente;
    private javax.swing.JMenuItem itemVeiculos;
    public javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenu menuCadastros;
    private javax.swing.JMenu menuFinanceiro;
    private javax.swing.JMenu menuOS;
    private javax.swing.JMenu menuRelatorioDeOS;
    private javax.swing.JMenu menuRelatorios;
    // End of variables declaration//GEN-END:variables
}
