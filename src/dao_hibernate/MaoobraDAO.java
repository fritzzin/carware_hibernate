package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Maoobra;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class MaoobraDAO extends GenericDAO{

    @Override
    public void listar(JTable tabela) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = HibernateUtil.getSessionFactory().openSession();
        Maoobra mo = (Maoobra) sessao.get(Maoobra.class, id);
        sessao.close();
        return mo;
    }
    @Override
    public boolean salvar(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.beginTransaction();

            sessao.save(obj);
            
            t.commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            return false;
        } finally {
            sessao.close();
        }
        return true;
    }
    
    
    //ordem [input] = desc,asc
    //criterio [input] = "xxxxx"
    //Não sei se está funcionando
    public void listar(JTable tabela, String criterio, String ordem) {
        List<Maoobra> listaMaoobra = null;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query q;

            if (criterio.trim().equals("") && ordem.equals("")) {
                // SEM ORDEM E SEM CRITERIO
                q = sessao.createQuery("from Maoobra ");
            } else if (criterio.trim().equals("")) {
                // SEM CRITERIO
                q = sessao.createQuery(""
                        + "from     Maoobra "
                        + "order by custo " + ordem);
            } else if (ordem.equals("")) {
                // SEM ORDEM
                q = sessao.createQuery(""
                        + "from     Produto "
                        + "where    lower(nome) like '%" + criterio + "%' ");
            } else {
                // NENHUM VAZIO
                q = sessao.createQuery(""
                        + "from     Produto "
                        + "where    lower(nome) like '%" + criterio + "%' "
                        + "order by custo " + ordem);
            }

            listaMaoobra = q.list();

        } catch (HibernateException he) {
            he.printStackTrace();
        }

        // Modelo de tabela e adicionar linhas a tabela
        DefaultTableModel modeloTabela = new DefaultTableModel();

        tabela.setModel(modeloTabela);

        modeloTabela.addColumn("Id");
        modeloTabela.addColumn("Descrição");
        modeloTabela.addColumn("Funcionario");
        modeloTabela.addColumn("Custo");
        modeloTabela.addColumn("Data");

        for (Maoobra maoobra : listaMaoobra) {
            modeloTabela.addRow(new Object[]{
                maoobra.getId(),
                maoobra.getDescricao(),
                maoobra.getFuncionario().getPessoa().getNome(),
                maoobra.getCusto(),
                maoobra.getData(),});
        }
    }
    
}
