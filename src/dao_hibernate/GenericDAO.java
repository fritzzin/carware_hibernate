package dao_hibernate;

import javax.swing.JTable;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import carware.HibernateUtil;

/**
 *
 * @author Augusto Fritz; Lucas Assis
 */
public abstract class GenericDAO {

    public boolean salvar(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.beginTransaction();

            sessao.saveOrUpdate(obj);

            t.commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            return false;
        } finally {
            sessao.close();
        }
        return true;
    }

    public boolean atualizar(Object obj) {
        return salvar(obj);
    }

    public boolean deletar(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.getTransaction();

            t.begin();
            sessao.delete(obj);
            t.commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            return false;
        } finally {
            sessao.close();
        }
        return true;
    }

    public abstract void listar(JTable tabela);

    public abstract Object consultaId(int id);
}