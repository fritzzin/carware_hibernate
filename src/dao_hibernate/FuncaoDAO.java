/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Funcao;
import javax.swing.JTable;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Augusto
 */
public class FuncaoDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();

            Funcao funcao = (Funcao) sessao.get(Funcao.class, id);
            return funcao;

        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        }
    }

    public Funcao consultarPorFuncao(String nome) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Query q = sessao.createQuery("from Funcao where nome = '" + nome + "'");
            q.setMaxResults(1);
            Funcao funcao = (Funcao) q.uniqueResult();
            return funcao;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }
}
