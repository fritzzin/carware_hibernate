package dao_hibernate;

import dao_hibernate.*;
import carware.HibernateUtil;
import classes_hibernate.Os;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class OsDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        List<Os> listaOs = null;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery("from Os "
                    + "order by status");

            listaOs = query.list();

        } catch (HibernateException he) {
            he.printStackTrace();
        }

        DefaultTableModel modeloTabela = new DefaultTableModel();

        tabela.setModel(modeloTabela);

        modeloTabela.addColumn("ID");
        modeloTabela.addColumn("Placa");
        modeloTabela.addColumn("Dono");
        modeloTabela.addColumn("Data de Entrada");
        modeloTabela.addColumn("Status");

        for (Os os : listaOs) {
            String status = null;
            switch (os.getStatus()) {
                case 'A':
                    status = "Orcamento";
                    break;
                case 'B':
                    status = "Em andamento";
                    break;
                case 'C':
                    status = "Aguardando entrega";
                    break;
                case 'D':
                    status = "Finalizada";
                    break;
            }

            modeloTabela.addRow(new Object[]{
                os.getId(),
                os.getVeiculopessoa().getVeiculo().getPlaca(),
                os.getVeiculopessoa().getPessoa().getNome(),
                os.getDataentrada(),
                status});
        }

        sessao.close();
    }

    public void lista(JTable tabela, String criterio, String tipo) {
        List<Os> listaOs = null;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery(""
                    + "from Os "
                    + "where " + tipo + " like '%" + criterio + "%' ");

            listaOs = query.list();

        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }

        DefaultTableModel modeloTabela = new DefaultTableModel();

        tabela.setModel(modeloTabela);

        modeloTabela.addColumn("Id");
        modeloTabela.addColumn("Veículo");
        modeloTabela.addColumn("Dono");
        modeloTabela.addColumn("Ano");
        modeloTabela.addColumn("Cor");
        modeloTabela.addColumn("Marca");
        modeloTabela.addColumn("Placa");

//        for (Os os : listaOs) {
//            modeloTabela.addRow(new Object[]{
//                os.getId(),
//                os.getVeiculopessoa(),});
//        }
    }

    public List<Os> listaOsGrafico() {
        List<Os> listaOs = null;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery("from Os "
                    + "where status != 'D'");

            listaOs = query.list();

        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }
        return listaOs;
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.getTransaction();

            t.begin();

            Os os = (Os) sessao.get(Os.class, id);

            t.commit();

            return os;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }
}
