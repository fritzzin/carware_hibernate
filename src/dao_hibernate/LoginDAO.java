package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Login;
import javax.swing.JTable;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Augusto Fritz; Lucas Assis
 */
public class LoginDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = HibernateUtil.getSessionFactory().openSession();
        Login l = (Login) sessao.get(Login.class, id);
        sessao.close();
        return l;
    }

    public boolean realizarLogin(Login l) {
        boolean acesso = false;
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            String hql = "  from    Login "
                    + "     where   login = '" + l.getLogin() + "' "
                    + "     AND     senha = '" + l.getSenha() + "'";

            Query query = sessao.createQuery(hql);
            query.setMaxResults(1);

            Login teste = (Login) query.uniqueResult();

            if (teste != null) {
                acesso = true;
                return acesso;
            }
        } catch (HibernateException he) {
            he.printStackTrace();
            return acesso;
        } finally {
            sessao.close();
        }
        return acesso;
    }

    public Object consultaporLogin(String login) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Query q = sessao.createQuery("from Login where login = '" + login + "'");
            q.setMaxResults(1);
            Login l = (Login) q.uniqueResult();
            return l;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }

    public boolean loginJaExiste(String login) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Query q = sessao.createQuery("from Login where login = '" + login + "'");
            q.setMaxResults(1);
            Login l = (Login) q.uniqueResult();

            if (l == null) {
                return false;
            } else {
                return true;
            }

        } catch (HibernateException he) {
            he.printStackTrace();
            return false;
        } finally {
            sessao.close();
        }
    }
}
