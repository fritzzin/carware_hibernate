/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao_hibernate;

import apoio.Formatacao;
import carware.HibernateUtil;
import classes_hibernate.Fornecedor;
import classes_hibernate.Fornecedorproduto;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author fritzzin
 */
public class FornecedorProdutoDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void listar(JTable tabela, String ordem) {
        List<Fornecedorproduto> lista = null;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery(""
                    + "from     Fornecedorproduto "
                    + "order by " + ordem);

            lista = query.list();

        } catch (HibernateException he) {
            he.printStackTrace();
        }

        try {
            if (!lista.isEmpty()) {
                // Modelo de tabela e adicionar linhas a tabela
                DefaultTableModel modeloTabela = new DefaultTableModel();

                tabela.setModel(modeloTabela);

                modeloTabela.addColumn("Id");
                modeloTabela.addColumn("Fornecedor");
                modeloTabela.addColumn("Produto");
                modeloTabela.addColumn("Data de Compra");
                modeloTabela.addColumn("Quantidade");
                modeloTabela.addColumn("Valor Unitário");
                modeloTabela.addColumn("Data de Vencimento");
                modeloTabela.addColumn("Status");

                for (Fornecedorproduto fornecedorProduto : lista) {
                    String status = "";
                    if (fornecedorProduto.isStatus()) {
                        status = "Pago";
                    } else {
                        status = "Não Pago";
                    }

                    modeloTabela.addRow(new Object[]{
                        fornecedorProduto.getId(),
                        fornecedorProduto.getFornecedor().getNome(),
                        fornecedorProduto.getProduto().getNome(),
                        Formatacao.ajustaDataDMA(fornecedorProduto.getData().toString()),
                        fornecedorProduto.getQuantidade(),
                        fornecedorProduto.getValorunitario(),
                        Formatacao.ajustaDataDMA(fornecedorProduto.getDatavencimento().toString()),
                        status
                    });
                }
            } else {
                System.out.println("Não há mais registros para carregar");
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sessao.close();
        }

    }

    @Override
    public Object consultaId(int id) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.getTransaction();

            t.begin();

            Fornecedorproduto fp = (Fornecedorproduto) sessao.get(Fornecedorproduto.class, id);
            fp.getProduto().toString();
            fp.getFornecedor().toString();
            t.commit();

            return fp;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }

    public List listar() {
        List<Fornecedorproduto> listaDeFornecedores = null;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery(""
                    + "from     Fornecedorproduto ");

            listaDeFornecedores = query.list();
            return listaDeFornecedores;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }

}
