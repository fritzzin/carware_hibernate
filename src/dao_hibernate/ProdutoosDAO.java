package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Produto;
import classes_hibernate.Produtoos;
import java.math.BigDecimal;
import javax.swing.JTable;
import org.hibernate.Session;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class ProdutoosDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object consultaId(int id) {
        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            Produtoos p = (Produtoos) sessao.get(Produtoos.class, id);
            p.getProduto().toString();
            sessao.close();
            return p;
        } catch (Exception e) {
        }

        return null;
    }

    public void deletar(Produtoos prod) {
        try {
            Produto p = (Produto) new ProdutoDAO().consultaId(prod.getProduto().getId());
            int quantidade = prod.getQuantidade();
            Produto pd = prod.getProduto();
            BigDecimal novaQuantidade = p.getQuantidade();
            BigDecimal quantidadeAcrescentar = new BigDecimal(quantidade);
            novaQuantidade = quantidadeAcrescentar.add(novaQuantidade);
            p.setQuantidade(novaQuantidade);
            new ProdutoDAO().salvar(p);
            super.deletar(prod);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

}
