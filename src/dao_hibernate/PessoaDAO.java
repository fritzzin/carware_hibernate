package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Pessoa;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class PessoaDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();

            Pessoa pessoa = (Pessoa) sessao.get(Pessoa.class, id);
            
            return pessoa;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }

    public Pessoa consultarPorNome(String nome) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Query query = sessao.createQuery("from Pessoa where nome = '" + nome + "'");
            query.setMaxResults(1);
            Pessoa pessoa = (Pessoa) query.uniqueResult();
            return pessoa;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        }
    }
    
    public int pegaUltimo() {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query = null;

            query = sessao.createQuery(""
                    + "from     Pessoa "
                    + "order by id desc ");
            query.setMaxResults(1);
            Pessoa f = new Pessoa();
            f = (Pessoa) query.list().get(0);
            return f.getId();

        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }
        return -1;
    }

    public void populaTabelaPessoa(JTable tabela, String criterio, String tipo) {
        Session sessao = HibernateUtil.getSessionFactory().openSession();
        Query consulta = sessao.createQuery("from Pessoa");
        List<Pessoa> p = consulta.list();

        DefaultTableModel modeloTabela = new DefaultTableModel();
        tabela.setModel(modeloTabela);
        modeloTabela.addColumn("Código");
        modeloTabela.addColumn("Nome");
        modeloTabela.addColumn("Data Nascimento");
        modeloTabela.addColumn("Cpf");
        modeloTabela.addColumn("Telefone");
        modeloTabela.addColumn("Email");
        modeloTabela.addColumn("Status");
        for (Pessoa pessoa : p) {
            modeloTabela.addRow(new Object[]{
                pessoa.getId(),
                pessoa.getNome(),
                pessoa.getDatanascimento(),
                pessoa.getCpf(),
                pessoa.getTelefone(),
                pessoa.getEmail(),
                pessoa.isStatus()
            });
        }
    }

}
