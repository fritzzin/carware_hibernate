package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Cor;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class CorDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        List<Cor> listaCores = null;

        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;
            query = sessao.createQuery(""
                    + "from     Cor "
                    + "order by nome ");

            listaCores = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }

        DefaultTableModel modeloTabela = new DefaultTableModel();

        tabela.setModel(modeloTabela);

        modeloTabela.addColumn("id");
        modeloTabela.addColumn("nome");

        for (Cor cor : listaCores) {
            modeloTabela.addRow(new Object[]{
                cor.getId(),
                cor.getNome()});
        }
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.getTransaction();

            t.begin();
            Cor cor = (Cor) sessao.load(Cor.class, id);
            // Iniciar variavel?
            cor.getNome();

            t.commit();

            return cor;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }

}
