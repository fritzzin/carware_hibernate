package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Funcionario;
import java.util.List;
import javassist.convert.Transformer;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class FuncionarioDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.getTransaction();

            t.begin();

            Funcionario funcionario = (Funcionario) sessao.get(Funcionario.class, id);
            funcionario.getPessoa().toString();
            funcionario.getLogin().toString();
            funcionario.getFuncao().toString();
            t.commit();

            return funcionario;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }

    public void listar(JTable tabela, String criterio, String ordem) {
        List<Funcionario> listaFuncionario = null;
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery("from Funcionario");

            listaFuncionario = query.list();

        } catch (HibernateException he) {
            he.printStackTrace();
        }

        // Modelo de tabela e adicionar linhas a tabela
        DefaultTableModel modeloTabela = new DefaultTableModel();

        tabela.setModel(modeloTabela);

        modeloTabela.addColumn("Id");
        modeloTabela.addColumn("Funcionario");
        modeloTabela.addColumn("CTPS");
        modeloTabela.addColumn("Salário");
        modeloTabela.addColumn("Especialidade");
        modeloTabela.addColumn("Data de Admissão");
        modeloTabela.addColumn("Função");
        modeloTabela.addColumn("Login");

        for (Funcionario funcionario : listaFuncionario) {
            modeloTabela.addRow(new Object[]{
                funcionario.getIdpessoa(),
                funcionario.getPessoa().getNome(),
                funcionario.getCtps(),
                funcionario.getSalario(),
                funcionario.getEspecialidade(),
                funcionario.getDataadmissao(),
                funcionario.getFuncao().getNome(),
                funcionario.getLogin().getLogin()
            });
        }

        sessao.close();
    }

    public Object consultaporLogin(int idLogin) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();

            Query q = sessao.createQuery("from Funcionario where login = " + idLogin);
            q.setMaxResults(1);

            Funcionario f = (Funcionario) q.uniqueResult();
            f.getPessoa().toString();
            f.getLogin().toString();
            f.getFuncao().toString();
            return f;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }
}
