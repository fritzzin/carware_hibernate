package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Produto;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import tela.IfrProduto;

/**
 *
 * @author Augusto Fritz; Lucas Assis
 */
public class ProdutoDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
    }

    public void listarEntreValores(JTable tabela, String criterio, String ordem, int inicio, int fim, IfrProduto tela) {
        List<Produto> listaProduto = null;
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery(""
                    + "from Produto "
                    + "where id BETWEEN " + inicio + " AND " + fim + " "
                    + "order by id " + ordem);

            listaProduto = query.list();

        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }

        if (listaProduto.size() != 0) {
            tela.setChegouNoFinal(false);
            // Modelo de tabela e adicionar linhas a tabela
            DefaultTableModel modeloTabela = new DefaultTableModel();

            tabela.setModel(modeloTabela);

            modeloTabela.addColumn("Id");
            modeloTabela.addColumn("Nome");
            modeloTabela.addColumn("Valor Pago");
            modeloTabela.addColumn("Valor Venda");
            modeloTabela.addColumn("Quantidade");

            for (Produto produto : listaProduto) {
                modeloTabela.addRow(new Object[]{
                    produto.getId(),
                    produto.getNome(),
                    "R$" + produto.getValorpago(),
                    "R$" + produto.getValorvenda(),
                    produto.getQuantidade(),});
            }
        } else {
            tela.setChegouNoFinal(true);
            System.out.println("Não há mais registros para carregar");
        }
    }

    public void listar(JTable tabela, String criterio, String ordem) {
        List<Produto> listaProduto = null;
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            if (criterio.trim().equals("") && ordem.equals("")) {
                // SEM ORDEM E SEM CRITERIO
                query = sessao.createQuery(""
                        + "from     Produto "
                        + "order by nome ");
            } else if (criterio.trim().equals("")) {
                // SEM CRITERIO
                query = sessao.createQuery(""
                        + "from     Produto "
                        + "order by valorvenda " + ordem);
            } else if (ordem.equals("")) {
                // SEM ORDEM
                query = sessao.createQuery(""
                        + "from     Produto "
                        + "where    lower(nome) like '%" + criterio + "%' ");
            } else {
                // NENHUM VAZIO
                query = sessao.createQuery(""
                        + "from     Produto "
                        + "where    lower(nome) like '%" + criterio + "%' "
                        + "order by valorvenda " + ordem);
            }

            listaProduto = query.list();

        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }

        // Modelo de tabela e adicionar linhas a tabela
        DefaultTableModel modeloTabela = new DefaultTableModel();

        tabela.setModel(modeloTabela);

        modeloTabela.addColumn("Id");
        modeloTabela.addColumn("Nome");
        modeloTabela.addColumn("Valor Pago");
        modeloTabela.addColumn("Valor Venda");
        modeloTabela.addColumn("Quantidade");

        for (Produto produto : listaProduto) {
            modeloTabela.addRow(new Object[]{
                produto.getId(),
                produto.getNome(),
                "R$" + produto.getValorpago(),
                "R$" + produto.getValorvenda(),
                produto.getQuantidade(),});
        }
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.getTransaction();

            t.begin();

            Produto produto = (Produto) sessao.get(Produto.class, id);

            t.commit();

            return produto;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }
}
