/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao_hibernate;

import carware.HibernateUtil;
import classes_hibernate.Fornecedor;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author lucas.assis
 */
public class FornecedorDAO extends GenericDAO {

    @Override
    public void listar(JTable tabela) {
        List<Fornecedor> listaDeFornecedores = null;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery(""
                    + "from     Fornecedor "
                    + "order by nome");

            listaDeFornecedores = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }

        DefaultTableModel modeloTabela = new DefaultTableModel();

        tabela.setModel(modeloTabela);

        modeloTabela.addColumn("Id");
        modeloTabela.addColumn("Nome");
        modeloTabela.addColumn("CNPJ");
        modeloTabela.addColumn("Endereço");
        modeloTabela.addColumn("Telefone");
        modeloTabela.addColumn("Email");
        modeloTabela.addColumn("Status");

        for (Fornecedor fornecedor : listaDeFornecedores) {
            modeloTabela.addRow(new Object[]{
                fornecedor.getId(),
                fornecedor.getNome(),
                fornecedor.getCnpj(),
                fornecedor.getEndereco(),
                fornecedor.getNumtelefone(),
                fornecedor.getEmail(),
                fornecedor.isStatus()});
        }
    }
    

    public void listar(JTable tabela, String criterio, String ordem) {
        List<Fornecedor> listaDeFornecedores = null;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query;

            query = sessao.createQuery(""
                    + "from     Fornecedor "
                    + "order by id");

            listaDeFornecedores = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }

        DefaultTableModel modeloTabela = new DefaultTableModel();

        tabela.setModel(modeloTabela);

        modeloTabela.addColumn("Id");
        modeloTabela.addColumn("Nome");
        modeloTabela.addColumn("CNPJ");
        modeloTabela.addColumn("Endereço");
        modeloTabela.addColumn("Telefone");
        modeloTabela.addColumn("Email");
        modeloTabela.addColumn("Status");

        for (Fornecedor fornecedor : listaDeFornecedores) {
            modeloTabela.addRow(new Object[]{
                fornecedor.getId(),
                fornecedor.getNome(),
                fornecedor.getCnpj(),
                fornecedor.getEndereco(),
                fornecedor.getNumtelefone(),
                fornecedor.getEmail(),
                fornecedor.isStatus()});
        }
    }

    @Override
    public Object consultaId(int id) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.getTransaction();

            t.begin();

            Fornecedor produto = (Fornecedor) sessao.get(Fornecedor.class, id);

            t.commit();

            return produto;
        } catch (HibernateException he) {
            he.printStackTrace();
            return null;
        } finally {
            sessao.close();
        }
    }

    public int pegaUltimo() {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            org.hibernate.Query query = null;

            query = sessao.createQuery(""
                    + "from     Fornecedor "
                    + "order by id desc ");
            query.setMaxResults(1);
            Fornecedor f = new Fornecedor();
            f = (Fornecedor) query.list().get(0);
            return f.getId();

        } catch (HibernateException he) {
            he.printStackTrace();
        } finally {
            sessao.close();
        }
        return -1;
    }

}
