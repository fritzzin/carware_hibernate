/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "fornecedorproduto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fornecedorproduto.findAll", query = "SELECT f FROM Fornecedorproduto f")
    , @NamedQuery(name = "Fornecedorproduto.findById", query = "SELECT f FROM Fornecedorproduto f WHERE f.id = :id")
    , @NamedQuery(name = "Fornecedorproduto.findByData", query = "SELECT f FROM Fornecedorproduto f WHERE f.data = :data")
    , @NamedQuery(name = "Fornecedorproduto.findByQuantidade", query = "SELECT f FROM Fornecedorproduto f WHERE f.quantidade = :quantidade")
    , @NamedQuery(name = "Fornecedorproduto.findByValorunitario", query = "SELECT f FROM Fornecedorproduto f WHERE f.valorunitario = :valorunitario")
    , @NamedQuery(name = "Fornecedorproduto.findByDatavencimento", query = "SELECT f FROM Fornecedorproduto f WHERE f.datavencimento = :datavencimento")
    , @NamedQuery(name = "Fornecedorproduto.findByStatus", query = "SELECT f FROM Fornecedorproduto f WHERE f.status = :status")})
public class Fornecedorproduto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @Basic(optional = false)
    @Column(name = "valorunitario")
    private BigDecimal valorunitario;
    @Basic(optional = false)
    @Column(name = "datavencimento")
    @Temporal(TemporalType.DATE)
    private Date datavencimento;
    @Basic(optional = false)
    @Column(name = "status")
    private boolean status;
    @JoinColumn(name = "idfornecedor", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Fornecedor idfornecedor;
    @JoinColumn(name = "idproduto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Produto idproduto;

    public Fornecedorproduto() {
    }

    public Fornecedorproduto(Integer id) {
        this.id = id;
    }

    public Fornecedorproduto(Integer id, Date data, BigDecimal quantidade, BigDecimal valorunitario, Date datavencimento, boolean status) {
        this.id = id;
        this.data = data;
        this.quantidade = quantidade;
        this.valorunitario = valorunitario;
        this.datavencimento = datavencimento;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorunitario() {
        return valorunitario;
    }

    public void setValorunitario(BigDecimal valorunitario) {
        this.valorunitario = valorunitario;
    }

    public Date getDatavencimento() {
        return datavencimento;
    }

    public void setDatavencimento(Date datavencimento) {
        this.datavencimento = datavencimento;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Fornecedor getIdfornecedor() {
        return idfornecedor;
    }

    public void setIdfornecedor(Fornecedor idfornecedor) {
        this.idfornecedor = idfornecedor;
    }

    public Produto getIdproduto() {
        return idproduto;
    }

    public void setIdproduto(Produto idproduto) {
        this.idproduto = idproduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fornecedorproduto)) {
            return false;
        }
        Fornecedorproduto other = (Fornecedorproduto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Fornecedorproduto[ id=" + id + " ]";
    }
    
}
