/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "produtoos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produtoos.findAll", query = "SELECT p FROM Produtoos p")
    , @NamedQuery(name = "Produtoos.findById", query = "SELECT p FROM Produtoos p WHERE p.id = :id")
    , @NamedQuery(name = "Produtoos.findByValorunitario", query = "SELECT p FROM Produtoos p WHERE p.valorunitario = :valorunitario")
    , @NamedQuery(name = "Produtoos.findByData", query = "SELECT p FROM Produtoos p WHERE p.data = :data")})
public class Produtoos implements Serializable {

    @Basic(optional = false)
    @Column(name = "quantidade")
    private int quantidade;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "valorunitario")
    private BigDecimal valorunitario;
    @Basic(optional = false)
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @JoinColumn(name = "idos", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Os idos;
    @JoinColumn(name = "idproduto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Produto idproduto;

    public Produtoos() {
    }

    public Produtoos(Integer id) {
        this.id = id;
    }

    public Produtoos(Integer id, BigDecimal valorunitario, Date data) {
        this.id = id;
        this.valorunitario = valorunitario;
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValorunitario() {
        return valorunitario;
    }

    public void setValorunitario(BigDecimal valorunitario) {
        this.valorunitario = valorunitario;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Os getIdos() {
        return idos;
    }

    public void setIdos(Os idos) {
        this.idos = idos;
    }

    public Produto getIdproduto() {
        return idproduto;
    }

    public void setIdproduto(Produto idproduto) {
        this.idproduto = idproduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produtoos)) {
            return false;
        }
        Produtoos other = (Produtoos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Produtoos[ id=" + id + " ]";
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
}
