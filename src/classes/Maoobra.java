/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "maoobra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Maoobra.findAll", query = "SELECT m FROM Maoobra m")
    , @NamedQuery(name = "Maoobra.findById", query = "SELECT m FROM Maoobra m WHERE m.id = :id")
    , @NamedQuery(name = "Maoobra.findByCusto", query = "SELECT m FROM Maoobra m WHERE m.custo = :custo")
    , @NamedQuery(name = "Maoobra.findByData", query = "SELECT m FROM Maoobra m WHERE m.data = :data")
    , @NamedQuery(name = "Maoobra.findByDescricao", query = "SELECT m FROM Maoobra m WHERE m.descricao = :descricao")
    , @NamedQuery(name = "Maoobra.findByTempoinvestido", query = "SELECT m FROM Maoobra m WHERE m.tempoinvestido = :tempoinvestido")})
public class Maoobra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "custo")
    private BigDecimal custo;
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "tempoinvestido")
    private String tempoinvestido;
    @JoinColumn(name = "idfuncionario", referencedColumnName = "idpessoa")
    @ManyToOne(optional = false)
    private Funcionario idfuncionario;
    @JoinColumn(name = "idos", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Os idos;

    public Maoobra() {
    }

    public Maoobra(Integer id) {
        this.id = id;
    }

    public Maoobra(Integer id, BigDecimal custo, String descricao) {
        this.id = id;
        this.custo = custo;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getCusto() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo = custo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTempoinvestido() {
        return tempoinvestido;
    }

    public void setTempoinvestido(String tempoinvestido) {
        this.tempoinvestido = tempoinvestido;
    }

    public Funcionario getIdfuncionario() {
        return idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Os getIdos() {
        return idos;
    }

    public void setIdos(Os idos) {
        this.idos = idos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Maoobra)) {
            return false;
        }
        Maoobra other = (Maoobra) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Maoobra[ id=" + id + " ]";
    }
    
}
