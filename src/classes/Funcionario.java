/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "funcionario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f")
    , @NamedQuery(name = "Funcionario.findByIdpessoa", query = "SELECT f FROM Funcionario f WHERE f.idpessoa = :idpessoa")
    , @NamedQuery(name = "Funcionario.findByCtps", query = "SELECT f FROM Funcionario f WHERE f.ctps = :ctps")
    , @NamedQuery(name = "Funcionario.findBySalario", query = "SELECT f FROM Funcionario f WHERE f.salario = :salario")
    , @NamedQuery(name = "Funcionario.findByEspecialidade", query = "SELECT f FROM Funcionario f WHERE f.especialidade = :especialidade")
    , @NamedQuery(name = "Funcionario.findByDataadmissao", query = "SELECT f FROM Funcionario f WHERE f.dataadmissao = :dataadmissao")})
public class Funcionario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idpessoa")
    private Integer idpessoa;
    @Basic(optional = false)
    @Column(name = "ctps")
    private String ctps;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "salario")
    private BigDecimal salario;
    @Basic(optional = false)
    @Column(name = "especialidade")
    private String especialidade;
    @Basic(optional = false)
    @Column(name = "dataadmissao")
    @Temporal(TemporalType.DATE)
    private Date dataadmissao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idfuncionario")
    private Collection<Maoobra> maoobraCollection;
    @JoinColumn(name = "idfuncao", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Funcao idfuncao;
    @JoinColumn(name = "idlogin", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Login idlogin;
    @JoinColumn(name = "idpessoa", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Pessoa pessoa;

    public Funcionario() {
    }

    public Funcionario(Integer idpessoa) {
        this.idpessoa = idpessoa;
    }

    public Funcionario(Integer idpessoa, String ctps, BigDecimal salario, String especialidade, Date dataadmissao) {
        this.idpessoa = idpessoa;
        this.ctps = ctps;
        this.salario = salario;
        this.especialidade = especialidade;
        this.dataadmissao = dataadmissao;
    }

    public Integer getIdpessoa() {
        return idpessoa;
    }

    public void setIdpessoa(Integer idpessoa) {
        this.idpessoa = idpessoa;
    }

    public String getCtps() {
        return ctps;
    }

    public void setCtps(String ctps) {
        this.ctps = ctps;
    }

    public BigDecimal getSalario() {
        return salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public Date getDataadmissao() {
        return dataadmissao;
    }

    public void setDataadmissao(Date dataadmissao) {
        this.dataadmissao = dataadmissao;
    }

    @XmlTransient
    public Collection<Maoobra> getMaoobraCollection() {
        return maoobraCollection;
    }

    public void setMaoobraCollection(Collection<Maoobra> maoobraCollection) {
        this.maoobraCollection = maoobraCollection;
    }

    public Funcao getIdfuncao() {
        return idfuncao;
    }

    public void setIdfuncao(Funcao idfuncao) {
        this.idfuncao = idfuncao;
    }

    public Login getIdlogin() {
        return idlogin;
    }

    public void setIdlogin(Login idlogin) {
        this.idlogin = idlogin;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpessoa != null ? idpessoa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Funcionario)) {
            return false;
        }
        Funcionario other = (Funcionario) object;
        if ((this.idpessoa == null && other.idpessoa != null) || (this.idpessoa != null && !this.idpessoa.equals(other.idpessoa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Funcionario[ idpessoa=" + idpessoa + " ]";
    }
    
}
