/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "servicosbasicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servicosbasicos.findAll", query = "SELECT s FROM Servicosbasicos s")
    , @NamedQuery(name = "Servicosbasicos.findById", query = "SELECT s FROM Servicosbasicos s WHERE s.id = :id")
    , @NamedQuery(name = "Servicosbasicos.findByNome", query = "SELECT s FROM Servicosbasicos s WHERE s.nome = :nome")
    , @NamedQuery(name = "Servicosbasicos.findByDescricao", query = "SELECT s FROM Servicosbasicos s WHERE s.descricao = :descricao")
    , @NamedQuery(name = "Servicosbasicos.findByValor", query = "SELECT s FROM Servicosbasicos s WHERE s.valor = :valor")})
public class Servicosbasicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "valor")
    private BigDecimal valor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idservicosbasicos")
    private Collection<Osservicosbasicos> osservicosbasicosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idservicosbasicos")
    private Collection<Produtoservicosbasicos> produtoservicosbasicosCollection;

    public Servicosbasicos() {
    }

    public Servicosbasicos(Integer id) {
        this.id = id;
    }

    public Servicosbasicos(Integer id, String nome, String descricao, BigDecimal valor) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @XmlTransient
    public Collection<Osservicosbasicos> getOsservicosbasicosCollection() {
        return osservicosbasicosCollection;
    }

    public void setOsservicosbasicosCollection(Collection<Osservicosbasicos> osservicosbasicosCollection) {
        this.osservicosbasicosCollection = osservicosbasicosCollection;
    }

    @XmlTransient
    public Collection<Produtoservicosbasicos> getProdutoservicosbasicosCollection() {
        return produtoservicosbasicosCollection;
    }

    public void setProdutoservicosbasicosCollection(Collection<Produtoservicosbasicos> produtoservicosbasicosCollection) {
        this.produtoservicosbasicosCollection = produtoservicosbasicosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicosbasicos)) {
            return false;
        }
        Servicosbasicos other = (Servicosbasicos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Servicosbasicos[ id=" + id + " ]";
    }
    
}
