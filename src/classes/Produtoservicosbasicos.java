/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "produtoservicosbasicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produtoservicosbasicos.findAll", query = "SELECT p FROM Produtoservicosbasicos p")
    , @NamedQuery(name = "Produtoservicosbasicos.findById", query = "SELECT p FROM Produtoservicosbasicos p WHERE p.id = :id")})
public class Produtoservicosbasicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "idproduto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Produto idproduto;
    @JoinColumn(name = "idservicosbasicos", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Servicosbasicos idservicosbasicos;

    public Produtoservicosbasicos() {
    }

    public Produtoservicosbasicos(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Produto getIdproduto() {
        return idproduto;
    }

    public void setIdproduto(Produto idproduto) {
        this.idproduto = idproduto;
    }

    public Servicosbasicos getIdservicosbasicos() {
        return idservicosbasicos;
    }

    public void setIdservicosbasicos(Servicosbasicos idservicosbasicos) {
        this.idservicosbasicos = idservicosbasicos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produtoservicosbasicos)) {
            return false;
        }
        Produtoservicosbasicos other = (Produtoservicosbasicos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Produtoservicosbasicos[ id=" + id + " ]";
    }
    
}
