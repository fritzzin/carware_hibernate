/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "veiculopessoa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Veiculopessoa.findAll", query = "SELECT v FROM Veiculopessoa v")
    , @NamedQuery(name = "Veiculopessoa.findById", query = "SELECT v FROM Veiculopessoa v WHERE v.id = :id")
    , @NamedQuery(name = "Veiculopessoa.findByData", query = "SELECT v FROM Veiculopessoa v WHERE v.data = :data")})
public class Veiculopessoa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idveiculopessoa")
    private Collection<Os> osCollection;
    @JoinColumn(name = "idpessoa", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pessoa idpessoa;
    @JoinColumn(name = "idveiculo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Veiculo idveiculo;

    public Veiculopessoa() {
    }

    public Veiculopessoa(Integer id) {
        this.id = id;
    }

    public Veiculopessoa(Integer id, Date data) {
        this.id = id;
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @XmlTransient
    public Collection<Os> getOsCollection() {
        return osCollection;
    }

    public void setOsCollection(Collection<Os> osCollection) {
        this.osCollection = osCollection;
    }

    public Pessoa getIdpessoa() {
        return idpessoa;
    }

    public void setIdpessoa(Pessoa idpessoa) {
        this.idpessoa = idpessoa;
    }

    public Veiculo getIdveiculo() {
        return idveiculo;
    }

    public void setIdveiculo(Veiculo idveiculo) {
        this.idveiculo = idveiculo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Veiculopessoa)) {
            return false;
        }
        Veiculopessoa other = (Veiculopessoa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Veiculopessoa[ id=" + id + " ]";
    }
    
}
