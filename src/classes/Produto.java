/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas.assis
 */
@Entity
@Table(name = "produto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p")
    , @NamedQuery(name = "Produto.findById", query = "SELECT p FROM Produto p WHERE p.id = :id")
    , @NamedQuery(name = "Produto.findByNome", query = "SELECT p FROM Produto p WHERE p.nome = :nome")
    , @NamedQuery(name = "Produto.findByValorpago", query = "SELECT p FROM Produto p WHERE p.valorpago = :valorpago")
    , @NamedQuery(name = "Produto.findByValorvenda", query = "SELECT p FROM Produto p WHERE p.valorvenda = :valorvenda")
    , @NamedQuery(name = "Produto.findByQuantidade", query = "SELECT p FROM Produto p WHERE p.quantidade = :quantidade")})
public class Produto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "valorpago")
    private BigDecimal valorpago;
    @Basic(optional = false)
    @Column(name = "valorvenda")
    private BigDecimal valorvenda;
    @Basic(optional = false)
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproduto")
    private Collection<Fornecedorproduto> fornecedorprodutoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproduto")
    private Collection<Produtoos> produtoosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproduto")
    private Collection<Produtoservicosbasicos> produtoservicosbasicosCollection;

    public Produto() {
    }

    public Produto(Integer id) {
        this.id = id;
    }

    public Produto(Integer id, String nome, BigDecimal valorpago, BigDecimal valorvenda, BigDecimal quantidade) {
        this.id = id;
        this.nome = nome;
        this.valorpago = valorpago;
        this.valorvenda = valorvenda;
        this.quantidade = quantidade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getValorpago() {
        return valorpago;
    }

    public void setValorpago(BigDecimal valorpago) {
        this.valorpago = valorpago;
    }

    public BigDecimal getValorvenda() {
        return valorvenda;
    }

    public void setValorvenda(BigDecimal valorvenda) {
        this.valorvenda = valorvenda;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    @XmlTransient
    public Collection<Fornecedorproduto> getFornecedorprodutoCollection() {
        return fornecedorprodutoCollection;
    }

    public void setFornecedorprodutoCollection(Collection<Fornecedorproduto> fornecedorprodutoCollection) {
        this.fornecedorprodutoCollection = fornecedorprodutoCollection;
    }

    @XmlTransient
    public Collection<Produtoos> getProdutoosCollection() {
        return produtoosCollection;
    }

    public void setProdutoosCollection(Collection<Produtoos> produtoosCollection) {
        this.produtoosCollection = produtoosCollection;
    }

    @XmlTransient
    public Collection<Produtoservicosbasicos> getProdutoservicosbasicosCollection() {
        return produtoservicosbasicosCollection;
    }

    public void setProdutoservicosbasicosCollection(Collection<Produtoservicosbasicos> produtoservicosbasicosCollection) {
        this.produtoservicosbasicosCollection = produtoservicosbasicosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistence.Produto[ id=" + id + " ]";
    }
    
}
