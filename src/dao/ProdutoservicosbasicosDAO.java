/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import apoio.ConexaoBD;
import classes.Osservicosbasicos;
import classes.Produtoservicosbasicos;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author lucas.assis
 */
public class ProdutoservicosbasicosDAO {
    
    ResultSet resultadoQ = null;

    public static ArrayList<Osservicosbasicos> popularProdutoos(int id) {
        ResultSet resultadoQ = null;
        ArrayList<Osservicosbasicos> posb = new ArrayList<Osservicosbasicos>();
        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select id from produtoos\n"
                    + "where idos = " + id);

            while (resultadoQ.next()) {
                Osservicosbasicos psb = GenericDAO.consultaProdutoServicosID(resultadoQ.getInt("id"));
                posb.add(psb);
                lin++;
            }
            return posb;
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
            return null;
        }
    }
    
}
