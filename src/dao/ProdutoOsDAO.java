/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import apoio.ConexaoBD;
import classes.Produto;
import classes.Produtoos;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author lucas.assis
 */
public class ProdutoOsDAO {

    ResultSet resultadoQ = null;

    public static ArrayList<Produtoos> popularProdutoos(int id) {
        ResultSet resultadoQ = null;
        ArrayList<Produtoos> po = new ArrayList<Produtoos>();
        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select id from produtoos\n"
                    + "where idos = " + id);

            while (resultadoQ.next()) {
                Produtoos mo = GenericDAO.consultaProdutoOSID(resultadoQ.getInt("id"));
                po.add(mo);
                lin++;
            }
            return po;
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
            return null;
        }
    }
}
