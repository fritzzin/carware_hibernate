package dao;

import apoio.ConexaoBD;
import apoio.IDAO_T;
import classes.Login;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Augusto Fritz; Lucas Assis;
 */
public class LoginDAO implements IDAO_T<Login> {

    ResultSet resultadoQ = null;

    @Override
    public String salvar(Login o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String atualizar(Login o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Login> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Login> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Login consultarId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean login(String loginUsuario, String senhaUsuario) {
        Login l = new Login();
        boolean acesso = false;
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = "  SELECT  *                               \n"
                    + "     FROM    login                           \n"
                    + "     WHERE   login = '" + loginUsuario + "'  \n"
                    + "     AND     senha = '" + senhaUsuario + "'  \n";

            resultadoQ = st.executeQuery(sql);

            // Ler campo
            if (resultadoQ.next()) {
                acesso = true;

                int id = resultadoQ.getInt("id");
                String login = resultadoQ.getString("login");
                String senha = resultadoQ.getString("senha");

                l.setId(id);
                l.setLogin(login);
                l.setSenha(senha);
            }
        } catch (Exception e) {
            System.out.println("Erro ao realizar o login = " + e);
            e.printStackTrace();
            System.out.println("Aqui");
        }
        return acesso;
    }
}
