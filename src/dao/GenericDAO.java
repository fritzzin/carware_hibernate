/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Cor;
import classes.Maoobra;
import classes.Marca;
import classes.Modelo;
import classes.Os;
import classes.Osservicosbasicos;
import classes.Pessoa;
import classes.Produtoos;
import classes.Produtoservicosbasicos;
import classes.Veiculo;
import classes.Veiculopessoa;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author lucas.assis
 */
public class GenericDAO {

    //Salva e faz update se o objeto enviado conter um ID já existenten no banco
    public static boolean salva(Object o) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            Object ob = em.merge(o);
            o = ob;
            em.getTransaction().commit();
            em.close();
            emf.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int saveReturnID(Veiculo o) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            o = em.merge(o);
            em.getTransaction().commit();
            em.close();
            emf.close();
            return o.getId();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    
    public static int saveReturnID(Veiculopessoa o) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            o = em.merge(o);
            em.getTransaction().commit();
            em.close();
            emf.close();
            return o.getId();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    
    public static int saveReturnID(Modelo o) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            o = em.merge(o);
            em.getTransaction().commit();
            em.close();
            emf.close();
            return o.getId();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
        
    public static int saveReturnID(Marca o) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            o = em.merge(o);
            em.getTransaction().commit();
            em.close();
            emf.close();
            return o.getId();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    
    public static int saveReturnID(Os o) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            o = em.merge(o);
            em.getTransaction().commit();
            em.close();
            emf.close();
            return o.getId();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    public static int saveReturnID(Cor o) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            o = em.merge(o);
            em.getTransaction().commit();
            em.close();
            emf.close();
            return o.getId();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static boolean remove(Object o) {
        try {

            EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
            EntityManager em = emf.createEntityManager();

            em.getTransaction().begin();
            if (!em.contains(o)) {
                o = em.merge(o);
            }
            em.remove(o);
            em.getTransaction().commit();
            em.close();
            emf.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /*public static Object consultaMarcaID(Object o, int id){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(Object.class, id);
        return ob;
    }*/
    public static Marca consultaMarcaID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Marca m = em.find(Marca.class, id);
        return m;
    }

    public static Veiculo consultaVeiculoID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Veiculo v = em.find(Veiculo.class, id);
        return v;
    }

    public static Pessoa consultaPessoaID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Pessoa p = em.find(Pessoa.class, id);
        return p;
    }

    public static Modelo consultaModeloID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Modelo m = em.find(Modelo.class, id);
        return m;
    }

    public static Cor consultaCorID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Cor c = em.find(Cor.class, id);
        return c;
    }

    public static Veiculopessoa consultaVeiculoPessoaID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Veiculopessoa vp = em.find(Veiculopessoa.class, id);
        return vp;
    }

    public static Os consultaOSID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Os os = em.find(Os.class, id);
        //os.getIdveiculopessoa().getIdpessoa().toString();
        em.close();
        emf.close();
        return os;
    }
    
    public static Maoobra consultaMaoobraID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Maoobra mo = em.find(Maoobra.class, id);
        return mo;
    }
    
    public static Produtoos consultaProdutoOSID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Produtoos po = em.find(Produtoos.class, id);
        return po;
    }
    
    public static Osservicosbasicos consultaProdutoServicosID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("carwarePU");
        EntityManager em = emf.createEntityManager();
        Osservicosbasicos po = em.find(Osservicosbasicos.class, id);
        return po;
    }
}
