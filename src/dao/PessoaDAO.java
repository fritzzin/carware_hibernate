/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import apoio.ConexaoBD;
import apoio.IDAO_T;
import apoio.formataData;
import classes.Pessoa;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author lucas.assis
 */
public class PessoaDAO implements IDAO_T<Pessoa> {

    ResultSet resultadoQ = null;

    @Override
    public String salvar(Pessoa o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "select * from pessoa "
                    + "where  "
                    + "cpf = '" + o.getCpf() + "'";

            resultadoQ = st.executeQuery(sql);
            if (resultadoQ.next()) {
                return "CPF já existente!";
            }

            sql = ""
                    + "INSERT INTO pessoa VALUES ("
                    + "DEFAULT, "
                    + "'" + o.getNome() + "',"
                    + "'" + o.getDatanascimento()+ "',"
                    + "'" + o.getCpf() + "',"
                    + "'" + o.getTelefone() + "',"
                    + "'" + o.getEmail() + "',"
                    + "'" + o.getStatus() + "'"
                    + ")";

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro salvar pessoa = " + e);
            return e.toString();
        }
    }

    @Override
    public String atualizar(Pessoa o) {
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();
            String sql = ""
                    + "UPDATE pessoa "
                    + "SET "
                    + "nome = '" + o.getNome() + "',"
                    + "dataNascimento = '" + o.getDatanascimento() + "',"
                    + "cpf = '" + o.getCpf() + "', "
                    + "telefone = '" + o.getTelefone() + "', "
                    + "email = '" + o.getEmail() + "',"
                    + "status = '" + o.getStatus() + "' "
                    + "WHERE id = " + o.getId();

            System.out.println("sql: " + sql);

            int resultado = st.executeUpdate(sql);

            return null;
        } catch (Exception e) {
            System.out.println("Erro atualizar pessoa = " + e);
            return e.toString();
        }
    }

    @Override
    public String excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Pessoa> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Pessoa> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pessoa consultarId(int id) {
        Pessoa p = null;

        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();

            String sql = ""
                    + "select * from pessoa "
                    + "where  "
                    + "id = " + id;

            System.out.println("sql: " + sql);

            resultadoQ = st.executeQuery(sql);

            if (resultadoQ.next()) {
                p = new Pessoa();

                p.setId(id);
                p.setNome(resultadoQ.getString("nome"));
                p.setDatanascimento(resultadoQ.getDate("dataNascimento"));
                p.setCpf(resultadoQ.getString("cpf"));
                p.setTelefone(resultadoQ.getString("telefone"));
                p.setEmail(resultadoQ.getString("email"));
                p.setStatus(resultadoQ.getBoolean("status"));
            }

        } catch (Exception e) {
            System.out.println("Erro consultar pessoa = " + e);
        }
        return p;
    }

    //Ajeitar
    public void popularTabela (JTable tabela, String criterio, String tipo) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[7];
        cabecalho[0] = "Código";
        cabecalho[1] = "Nome";
        cabecalho[2] = "Data Nascimento";
        cabecalho[3] = "CPF";
        cabecalho[4] = "Telefone";
        cabecalho[5] = "Email";
        cabecalho[6] = "Status";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT count(*) FROM pessoa WHERE " + tipo + " ILIKE '%" + criterio + "%'");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][cabecalho.length];

        } catch (Exception e) {
            System.out.println("Erro ao consultar pessoa: " + e);
            System.out.println(""
                    + "SELECT count(*) FROM pessoa WHERE " + tipo + " ILIKE '%" + criterio + "%'");
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "SELECT * FROM pessoa WHERE " + tipo + " ILIKE '%" + criterio + "%' "
                    + "ORDER BY nome");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("nome");
                dadosTabela[lin][2] = formataData.formatarData(resultadoQ.getString("dataNascimento"));
                dadosTabela[lin][3] = resultadoQ.getString("cpf");
                dadosTabela[lin][4] = resultadoQ.getString("telefone");
                dadosTabela[lin][5] = resultadoQ.getString("email");
                boolean status = resultadoQ.getBoolean("status");
                if (status) {
                    dadosTabela[lin][6] = "Ativo";
                } else {
                    dadosTabela[lin][6] = "Inativo";
                }

                // caso a coluna precise exibir uma imagem
//                if (resultadoQ.getBoolean("Situacao")) {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_ativo.png"));
//                } else {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_inativo.png"));
//                }
                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
            System.out.println(""
                    + "SELECT * FROM pessoa WHERE " + tipo + " ILIKE '%" + criterio + "%' "
                    + "ORDER BY nome");
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
                /*  
                 if (column == 3) {  // apenas a coluna 3 sera editavel
                 return true;
                 } else {
                 return false;
                 }
                 */
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
        // renderizacao das linhas da tabela = mudar a cor
//        jTable1.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
//
//            @Override
//            public Component getTableCellRendererComponent(JTable table, Object value,
//                    boolean isSelected, boolean hasFocus, int row, int column) {
//                super.getTableCellRendererComponent(table, value, isSelected,
//                        hasFocus, row, column);
//                if (row % 2 == 0) {
//                    setBackground(Color.GREEN);
//                } else {
//                    setBackground(Color.LIGHT_GRAY);
//                }
//                return this;
//            }
//        });
    }
    
    
    
}
