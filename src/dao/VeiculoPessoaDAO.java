/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import apoio.ConexaoBD;
import apoio.IDAO_T;
import classes.Veiculopessoa;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author lucas.assis
 */
public class VeiculoPessoaDAO implements IDAO_T<Veiculopessoa> {

    ResultSet resultadoQ = null;

    @Override
    public String salvar(Veiculopessoa o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String atualizar(Veiculopessoa o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Veiculopessoa> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Veiculopessoa> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Veiculopessoa consultarId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //tipo = p.nome, mo.nome
    public void popularTabela(JTable tabela, String criterio, String tipo) {
        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[6];
        cabecalho[0] = "Código";
        cabecalho[1] = "Pessoa";
        cabecalho[2] = "Modelo";
        cabecalho[3] = "Ano";
        cabecalho[4] = "Cor";
        cabecalho[5] = "Marca";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select count(*)\n"
                    + "from marca mr, modelo mo, cor c, veiculo v, veiculopessoa vp, pessoa p\n"
                    + "where mr.id = mo.idmarca\n"
                    + "and mo.id = v.idmodelo\n"
                    + "and c.id = v.idcor\n"
                    + "and v.id = vp.idveiculo\n"
                    + "and vp.idpessoa = p.id\n"
                    + "and " + tipo + " ILIKE '%" + criterio + "%'");

            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][cabecalho.length];

        } catch (Exception e) {
            System.out.println("Erro ao consultar veiculopessoa: " + e);
            System.out.println(""
                    + "select count(*)\n"
                    + "from marca mr, modelo mo, cor c, veiculo v, veiculopessoa vp, pessoa p\n"
                    + "where mr.id = mo.idmarca\n"
                    + "and mo.id = v.idmodelo\n"
                    + "and c.id = v.idcor\n"
                    + "and v.id = vp.idveiculo\n"
                    + "and vp.idpessoa = p.id\n"
                    + "and " + tipo + " ILIKE '%" + criterio + "%'");
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select vp.id, p.nome AS \"pessoa\", mo.nome AS \"modelo\", mo.ano, c.nome AS \"cor\", mr.nome AS \"marca\"\n"
                    + "from marca mr, modelo mo, cor c, veiculo v, veiculopessoa vp, pessoa p\n"
                    + "where mr.id = mo.idmarca\n"
                    + "and mo.id = v.idmodelo\n"
                    + "and c.id = v.idcor\n"
                    + "and v.id = vp.idveiculo\n"
                    + "and vp.idpessoa = p.id\n"
                    + "and " + tipo + " ILIKE '%" + criterio + "%'");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("pessoa");
                dadosTabela[lin][2] = resultadoQ.getString("modelo");
                dadosTabela[lin][3] = resultadoQ.getInt("ano");
                dadosTabela[lin][4] = resultadoQ.getString("cor");
                dadosTabela[lin][5] = resultadoQ.getString("marca");

                // caso a coluna precise exibir uma imagem
//                if (resultadoQ.getBoolean("Situacao")) {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_ativo.png"));
//                } else {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_inativo.png"));
//                }
                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
            System.out.println(""
                    + "select vp.id, p.nome AS \"pessoa\", mo.nome AS \"modelo\", mo.ano, c.nome AS \"cor\", mr.nome AS \"marca\"\n"
                    + "from marca mr, modelo mo, cor c, veiculo v, veiculopessoa vp, pessoa p\n"
                    + "where mr.id = mo.idmarca\n"
                    + "and mo.id = v.idmodelo\n"
                    + "and c.id = v.idcor\n"
                    + "and v.id = vp.idveiculo\n"
                    + "and vp.idpessoa = p.id\n"
                    + "and " + tipo + " ILIKE '%" + criterio + "%'");
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
                /*  
                 if (column == 3) {  // apenas a coluna 3 sera editavel
                 return true;
                 } else {
                 return false;
                 }
                 */
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
        // renderizacao das linhas da tabela = mudar a cor
//        jTable1.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
//
//            @Override
//            public Component getTableCellRendererComponent(JTable table, Object value,
//                    boolean isSelected, boolean hasFocus, int row, int column) {
//                super.getTableCellRendererComponent(table, value, isSelected,
//                        hasFocus, row, column);
//                if (row % 2 == 0) {
//                    setBackground(Color.GREEN);
//                } else {
//                    setBackground(Color.LIGHT_GRAY);
//                }
//                return this;
//            }
//        });
    }

}
