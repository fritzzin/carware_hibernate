/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import apoio.ConexaoBD;
import apoio.IDAO_T;
import classes.Os;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author lucas.assis
 */
public class OsDAO implements IDAO_T<Os> {

    ResultSet resultadoQ = null;

    public void popularTabela(JTable tabela, String criterio, String tipo, char c) {
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[7];
        cabecalho[0] = "Id";
        cabecalho[1] = "Veiculo";
        cabecalho[2] = "Dono";
        cabecalho[3] = "Ano";
        cabecalho[4] = "Cor";
        cabecalho[5] = "Marca";
        cabecalho[6] = "Placa";

        // cria matriz de acordo com nº de registros da tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "Select count(*)\n"
                    + "from pessoa, veiculopessoa, veiculo, modelo, cor, marca, os\n"
                    + "where pessoa.id = veiculopessoa.idpessoa\n"
                    + "and marca.id = modelo.idmarca\n"
                    + "and modelo.id = veiculo.idmodelo\n"
                    + "and cor.id = veiculo.idcor\n"
                    + "and veiculo.id = veiculopessoa.idveiculo\n"
                    + "and os.idveiculopessoa = veiculopessoa.id\n"
                    + "and os.status = '"+c+"'\n"
                    + "and " + tipo + " ILIKE '%" + criterio + "%'");

            System.out.println(""
                    + "Select count(*)\n"
                    + "from pessoa, veiculopessoa, veiculo, modelo, cor, marca, os\n"
                    + "where pessoa.id = veiculopessoa.idpessoa\n"
                    + "and marca.id = modelo.idmarca\n"
                    + "and modelo.id = veiculo.idmodelo\n"
                    + "and cor.id = veiculo.idcor\n"
                    + "and veiculo.id = veiculopessoa.idveiculo\n"
                    + "and os.idveiculopessoa = veiculopessoa.id\n"
                    + "and os.status = '"+c+"'\n"
                    + "and " + tipo + " ILIKE '%" + criterio + "%'");
            resultadoQ.next();

            dadosTabela = new Object[resultadoQ.getInt(1)][cabecalho.length];

        } catch (Exception e) {
            System.out.println("Erro ao consultar pessoa: " + e);
            System.out.println(""
                    + "Select count(*)\n"
                    + "from pessoa, veiculopessoa, veiculo, modelo, cor, marca, os\n"
                    + "where pessoa.id = veiculopessoa.idpessoa\n"
                    + "and marca.id = modelo.idmarca\n"
                    + "and modelo.id = veiculo.idmodelo\n"
                    + "and cor.id = veiculo.idcor\n"
                    + "and veiculo.id = veiculopessoa.idveiculo\n"
                    + "and os.idveiculopessoa = veiculopessoa.id\n"
                    + "and os.status = '"+c+"'\n"
                    + "and " + tipo + " ILIKE '%" + criterio + "%'");
        }

        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "Select os.id, modelo.nome AS \"modelo\", pessoa.nome AS \"nome\", modelo.ano, cor.nome AS \"cor\", marca.nome AS \"marca\", veiculo.placa\n"
                    + "from pessoa, veiculopessoa, veiculo, modelo, cor, marca, os\n"
                    + "where pessoa.id = veiculopessoa.idpessoa\n"
                    + "and marca.id = modelo.idmarca\n"
                    + "and modelo.id = veiculo.idmodelo\n"
                    + "and cor.id = veiculo.idcor\n"
                    + "and veiculo.id = veiculopessoa.idveiculo\n"
                    + "and os.idveiculopessoa = veiculopessoa.id\n"
                    + "and " + tipo + " ilike '%" + criterio + "%'\n"
                    + "and os.status = '"+c+"'");

            while (resultadoQ.next()) {

                dadosTabela[lin][0] = resultadoQ.getInt("id");
                dadosTabela[lin][1] = resultadoQ.getString("modelo");
                dadosTabela[lin][2] = resultadoQ.getString("nome");
                dadosTabela[lin][3] = resultadoQ.getInt("ano");
                dadosTabela[lin][4] = resultadoQ.getString("cor");
                dadosTabela[lin][5] = resultadoQ.getString("marca");
                dadosTabela[lin][6] = resultadoQ.getString("placa");

                // caso a coluna precise exibir uma imagem
//                if (resultadoQ.getBoolean("Situacao")) {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_ativo.png"));
//                } else {
//                    dadosTabela[lin][2] = new ImageIcon(getClass().getClassLoader().getResource("Interface/imagens/status_inativo.png"));
//                }
                lin++;
            }
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            e.printStackTrace();
            System.out.println(""
                    + "Select os.id, modelo.nome AS \"modelo\", pessoa.nome AS \"nome\", modelo.ano, cor.nome AS \"cor\", marca.nome AS \"marca\", veiculo.placa\n"
                    + "from pessoa, veiculopessoa, veiculo, modelo, cor, marca, os\n"
                    + "where pessoa.id = veiculopessoa.idpessoa\n"
                    + "and marca.id = modelo.idmarca\n"
                    + "and modelo.id = veiculo.idmodelo\n"
                    + "and cor.id = veiculo.idcor\n"
                    + "and veiculo.id = veiculopessoa.idveiculo\n"
                    + "and os.idveiculopessoa = veiculopessoa.id\n"
                    + "and " + tipo + " ilike '%" + criterio + "%'\n"
                    + "and os.status = '"+c+"'");
        }

        // configuracoes adicionais no componente tabela
        tabela.setModel(new DefaultTableModel(dadosTabela, cabecalho) {
            @Override
            // quando retorno for FALSE, a tabela nao é editavel
            public boolean isCellEditable(int row, int column) {
                return false;
                /*  
                 if (column == 3) {  // apenas a coluna 3 sera editavel
                 return true;
                 } else {
                 return false;
                 }
                 */
            }

            // alteracao no metodo que determina a coluna em que o objeto ImageIcon devera aparecer
            @Override
            public Class getColumnClass(int column) {

                if (column == 2) {
//                    return ImageIcon.class;
                }
                return Object.class;
            }
        });

        // permite seleção de apenas uma linha da tabela
        tabela.setSelectionMode(0);

        // redimensiona as colunas de uma tabela
        TableColumn column = null;
        for (int i = 0; i < tabela.getColumnCount(); i++) {
            column = tabela.getColumnModel().getColumn(i);
            switch (i) {
                case 0:
                    column.setPreferredWidth(17);
                    break;
                case 1:
                    column.setPreferredWidth(140);
                    break;
//                case 2:
//                    column.setPreferredWidth(14);
//                    break;
            }
        }
        // renderizacao das linhas da tabela = mudar a cor
//        jTable1.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
//
//            @Override
//            public Component getTableCellRendererComponent(JTable table, Object value,
//                    boolean isSelected, boolean hasFocus, int row, int column) {
//                super.getTableCellRendererComponent(table, value, isSelected,
//                        hasFocus, row, column);
//                if (row % 2 == 0) {
//                    setBackground(Color.GREEN);
//                } else {
//                    setBackground(Color.LIGHT_GRAY);
//                }
//                return this;
//            }
//        });
    }

    @Override
    public String salvar(Os o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String atualizar(Os o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String excluir(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Os> consultarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Os> consultar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Os consultarId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
