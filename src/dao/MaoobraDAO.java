/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import apoio.ConexaoBD;
import apoio.formataData;
import classes.Maoobra;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author lucas.assis
 */
public class MaoobraDAO {

    ResultSet resultadoQ = null;

    public static ArrayList<Maoobra> popularMaoObra(int id) {
        ResultSet resultadoQ = null;
        ArrayList<Maoobra> m = new ArrayList<Maoobra>();
        int lin = 0;

        // efetua consulta na tabela
        try {
            resultadoQ = ConexaoBD.getInstance().getConnection().createStatement().executeQuery(""
                    + "select id from maoobra\n"
                    + "where idos = " + id);

            while (resultadoQ.next()) {
                Maoobra mo = GenericDAO.consultaMaoobraID(resultadoQ.getInt("id"));
                m.add(mo);
                lin++;
            }
            return m;
        } catch (Exception e) {
            System.out.println("problemas para popular tabela...");
            System.out.println(e);
            return null;
        }
    }
}
