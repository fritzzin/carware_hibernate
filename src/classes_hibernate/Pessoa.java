package classes_hibernate;
// Generated Oct 31, 2019 2:23:24 AM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Pessoa generated by hbm2java
 */
@Entity
@Table(name="pessoa"
    ,schema="public"
)
public class Pessoa  implements java.io.Serializable {


     private int id;
     private String nome;
     private Date datanascimento;
     private String cpf;
     private String telefone;
     private String email;
     private boolean status;
     private Set veiculopessoas = new HashSet(0);
     private Funcionario funcionario;

    public Pessoa() {
    }

	
    public Pessoa(int id, String nome, Date datanascimento, String cpf, String telefone, String email, boolean status) {
        this.id = id;
        this.nome = nome;
        this.datanascimento = datanascimento;
        this.cpf = cpf;
        this.telefone = telefone;
        this.email = email;
        this.status = status;
    }
    public Pessoa(int id, String nome, Date datanascimento, String cpf, String telefone, String email, boolean status, Set veiculopessoas, Funcionario funcionario) {
       this.id = id;
       this.nome = nome;
       this.datanascimento = datanascimento;
       this.cpf = cpf;
       this.telefone = telefone;
       this.email = email;
       this.status = status;
       this.veiculopessoas = veiculopessoas;
       this.funcionario = funcionario;
    }
   
     @Id 

     
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    
    @Column(name="nome", nullable=false)
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="datanascimento", nullable=false, length=13)
    public Date getDatanascimento() {
        return this.datanascimento;
    }
    
    public void setDatanascimento(Date datanascimento) {
        this.datanascimento = datanascimento;
    }

    
    @Column(name="cpf", nullable=false, length=14)
    public String getCpf() {
        return this.cpf;
    }
    
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    
    @Column(name="telefone", nullable=false, length=15)
    public String getTelefone() {
        return this.telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    
    @Column(name="email", nullable=false)
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    
    @Column(name="status", nullable=false)
    public boolean isStatus() {
        return this.status;
    }
    
    public void setStatus(boolean status) {
        this.status = status;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="pessoa")
    public Set getVeiculopessoas() {
        return this.veiculopessoas;
    }
    
    public void setVeiculopessoas(Set veiculopessoas) {
        this.veiculopessoas = veiculopessoas;
    }

@OneToOne(fetch=FetchType.LAZY, mappedBy="pessoa")
    public Funcionario getFuncionario() {
        return this.funcionario;
    }
    
    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }




}


