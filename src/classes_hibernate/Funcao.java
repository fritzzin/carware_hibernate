package classes_hibernate;
// Generated Oct 31, 2019 2:23:24 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Funcao generated by hbm2java
 */
@Entity
@Table(name="funcao"
    ,schema="public"
)
public class Funcao  implements java.io.Serializable {


     private int id;
     private String nome;
     private String descricao;
     private Set funcionarios = new HashSet(0);

    public Funcao() {
    }

	
    public Funcao(int id, String nome, String descricao) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
    }
    public Funcao(int id, String nome, String descricao, Set funcionarios) {
       this.id = id;
       this.nome = nome;
       this.descricao = descricao;
       this.funcionarios = funcionarios;
    }
   
     @Id 

    
    @Column(name="id", unique=true, nullable=false)
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    
    @Column(name="nome", nullable=false)
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    @Column(name="descricao", nullable=false)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="funcao")
    public Set getFuncionarios() {
        return this.funcionarios;
    }
    
    public void setFuncionarios(Set funcionarios) {
        this.funcionarios = funcionarios;
    }




}


