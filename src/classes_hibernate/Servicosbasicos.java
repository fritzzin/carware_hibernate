package classes_hibernate;
// Generated Oct 31, 2019 2:23:24 AM by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Servicosbasicos generated by hbm2java
 */
@Entity
@Table(name="servicosbasicos"
    ,schema="public"
)
public class Servicosbasicos  implements java.io.Serializable {


     private int id;
     private String nome;
     private String descricao;
     private BigDecimal valor;
     private Set produtoservicosbasicoses = new HashSet(0);
     private Set osservicosbasicoses = new HashSet(0);

    public Servicosbasicos() {
    }

	
    public Servicosbasicos(int id, String nome, String descricao, BigDecimal valor) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
    }
    public Servicosbasicos(int id, String nome, String descricao, BigDecimal valor, Set produtoservicosbasicoses, Set osservicosbasicoses) {
       this.id = id;
       this.nome = nome;
       this.descricao = descricao;
       this.valor = valor;
       this.produtoservicosbasicoses = produtoservicosbasicoses;
       this.osservicosbasicoses = osservicosbasicoses;
    }
   
     @Id 

    
    @Column(name="id", unique=true, nullable=false)
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    
    @Column(name="nome", nullable=false)
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    @Column(name="descricao", nullable=false)
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    
    @Column(name="valor", nullable=false, precision=10)
    public BigDecimal getValor() {
        return this.valor;
    }
    
    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="servicosbasicos")
    public Set getProdutoservicosbasicoses() {
        return this.produtoservicosbasicoses;
    }
    
    public void setProdutoservicosbasicoses(Set produtoservicosbasicoses) {
        this.produtoservicosbasicoses = produtoservicosbasicoses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="servicosbasicos")
    public Set getOsservicosbasicoses() {
        return this.osservicosbasicoses;
    }
    
    public void setOsservicosbasicoses(Set osservicosbasicoses) {
        this.osservicosbasicoses = osservicosbasicoses;
    }




}


